﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Resources
{
    public class Subject
    {
        public Subject(string Num, string Time, string ClassNum, string Name, string Professor)
        {
            this.Num = Num;
            this.Time = Time;
            this.ClassNum = ClassNum;
            this.Name = Name;
            this.Professor = Professor;

            day = "";
        }
        public string Num { get; set; }
        public string Time { get; set; }
        public string ClassNum { get; set; }
        public string Name { get; set; }
        public string Professor { get; set; }
        public string day { get; set; }
    }

    public class Day
    {
        public string name { get; set; }
        public List<Subject> subjects { get; set; }
        public Day(string name, List<Subject> subjects)
        {
            this.name = name;
            this.subjects = subjects;
        }
    }

    public class ScheduleFile
    {
        public ScheduleFile(string result, List<Day> days)
        {
            this.result = result;
            this.days = days;
        }
        public string result { get; set; }
        public List<Day> days { get; set; }
    }
}
