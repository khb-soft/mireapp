﻿using Microsoft.Phone.Controls;
using Microsoft.Phone.Net.NetworkInformation;
using Microsoft.Phone.Shell;
using Resources.Resources;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.IsolatedStorage;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using Windows.Storage;
using Windows.Storage.Streams;

namespace Resources
{
    public static class Helpers
    {
        public static string getApplicationVersion()
        {
            var ver = Windows.ApplicationModel.Package.Current.Id.Version;
            return ver.Major.ToString() + "." + ver.Minor.ToString() + "." + ver.Build.ToString() + "." + ver.Revision.ToString();
        }

        public static String ColorToHTML(Color color)
        {
            string html = color.ToString();
            html = html.Remove(1, 2);
            //MessageBox.Show(html);
            return html;
        }

        public static String getDeviceId()
        {
            byte[] id = (byte[])Microsoft.Phone.Info.DeviceExtendedProperties.GetValue("DeviceUniqueId");
            return BitConverter.ToString(id).Replace("-", string.Empty);
        }

        public static bool CheckNetworkConnection()
        {
            var ni = NetworkInterface.NetworkInterfaceType;

            bool IsConnected = false;
            if ((ni == NetworkInterfaceType.Wireless80211) || (ni == NetworkInterfaceType.MobileBroadbandCdma) || (ni == NetworkInterfaceType.MobileBroadbandGsm))
                IsConnected = true;
            else if (ni == NetworkInterfaceType.None)
                IsConnected = false;
            return IsConnected;
        }

        public static void SaveFile(string filename, string content)
        {
            IsolatedStorageFile fileStorage = IsolatedStorageFile.GetUserStoreForApplication();
            StreamWriter Writer = new StreamWriter(new IsolatedStorageFileStream(filename, FileMode.OpenOrCreate, fileStorage));
            Writer.Write(content);
            Writer.Close();
        }

        public static async Task<string> WriteTextFile(string filename, string contents)
        {
            StorageFolder localFolder = ApplicationData.Current.LocalFolder;
            StorageFile textFile = await localFolder.CreateFileAsync(filename, CreationCollisionOption.ReplaceExisting);

            using (IRandomAccessStream textStream = await textFile.OpenAsync(FileAccessMode.ReadWrite))
            {
                using (DataWriter textWriter = new DataWriter(textStream))
                {
                    textWriter.WriteString(contents);
                    await textWriter.StoreAsync();
                }
            }

            return textFile.Path;
        }

        public static async Task RemoveAllFiles()
        {
            IReadOnlyList<StorageFile> thefiles;

            try
            {
                var localFolder = Windows.Storage.ApplicationData.Current.LocalFolder;
                thefiles = await localFolder.GetFilesAsync();

                for (int i = 0; i < thefiles.Count; i++)
                {
                    await thefiles[i].DeleteAsync(StorageDeleteOption.Default);
                }
            }
            catch
            { }
        }

        public static async Task<string> ReadTextFile(string filename)
        {
            string contents;

            StorageFolder localFolder = ApplicationData.Current.LocalFolder;
            StorageFile textFile = await localFolder.GetFileAsync(filename);

            using (IRandomAccessStream textStream = await textFile.OpenReadAsync())
            {
                using (DataReader textReader = new DataReader(textStream))
                {
                    uint textLength = (uint)textStream.Size;
                    await textReader.LoadAsync(textLength);
                    contents = textReader.ReadString(textLength);
                }
            }
            return contents;
        }

        public static bool CheckFile(string filename)
        {
            IsolatedStorageFile fileStorage = IsolatedStorageFile.GetUserStoreForApplication();
            return fileStorage.FileExists(filename);
        }

        public static string LoadFile(string filename)
        {
            IsolatedStorageFile fileStorage = IsolatedStorageFile.GetUserStoreForApplication();
            StreamReader Reader = new StreamReader(new IsolatedStorageFileStream(filename, FileMode.OpenOrCreate, fileStorage));
            string filecontent = Reader.ReadToEnd();
            Reader.Close();
            return filecontent;
        }

        public static String OptimizeHTMLForWP(string html, bool fullscreen)
        {
            Color currentPhoneAccentColorHex = (Color)Application.Current.Resources["PhoneAccentColor"];
            Color currentPhoneBackgroundColorHex = (Color)Application.Current.Resources["PhoneBackgroundColor"];
            Color currentPhoneForegroundColorHex = (Color)Application.Current.Resources["PhoneForegroundColor"];

            // Script that will call raise the ScriptNotify via window.external.Notify
            string notifyJS = @"<script type='text/javascript' language='javascript'>
                                window.onload = function() {
                                    var links = document.getElementsByTagName('a');
                                    for(var i=0;i<links.length;i++) {
                                        links[i].onclick = function() {
                                            window.external.Notify(this.href);
                                        }
                                    }
                                }
                            </script>";

            // Inject the Javascript into the head section of the HTML document
            html = html + notifyJS;

            string optHtml;
            if (!fullscreen)
                optHtml = "<meta name='viewport' content='width=480, height=800, user-scalable=yes' /><style> .PhoneAccentColor { color: " + ColorToHTML(currentPhoneAccentColorHex) + "; }  .PhoneBackgroundColor { color: " + ColorToHTML(currentPhoneBackgroundColorHex) + "; }  .PhonForegroundColor { color: " + ColorToHTML(currentPhoneForegroundColorHex) + "; } body { font-size: 1.2em; background-color: " + ColorToHTML(currentPhoneBackgroundColorHex) + "; font-family: Segoe WP, segoe; color: " + ColorToHTML(currentPhoneForegroundColorHex) + "; margin: 0;} a { color: " + ColorToHTML(currentPhoneAccentColorHex) + "; text-decoration: underline; }</style> " + html;
            else
                optHtml = "<meta name='viewport' content='width=480, height=800, user-scalable=yes' /><style> .PhoneAccentColor { color: " + ColorToHTML(currentPhoneAccentColorHex) + "; }  .PhoneBackgroundColor { color: " + ColorToHTML(currentPhoneBackgroundColorHex) + "; }  .PhonForegroundColor { color: " + ColorToHTML(currentPhoneForegroundColorHex) + "; } body { font-size: 1.2em; background-color: " + ColorToHTML(currentPhoneBackgroundColorHex) + "; font-family: Segoe WP, segoe; color: " + ColorToHTML(currentPhoneForegroundColorHex) + "; } a { color: " + ColorToHTML(currentPhoneAccentColorHex) + "; text-decoration: underline; }</style> " + html;
            return optHtml;
        }

        public static string ConvertExtendedASCII(string HTML)
        {
            string retVal = "";
            char[] s = HTML.ToCharArray();

            foreach (char c in s)
            {
                if (Convert.ToInt32(c) > 127)
                    retVal += "&#" + Convert.ToInt32(c) + ";";
                else
                    retVal += c;
            }

            return retVal;
        }

        public static void UpdateTile(News news)
        {
            bool useBackgroundAgent = true;

            if (IsolatedStorageSettings.ApplicationSettings.Contains("userData.settings.backgroundagent.enabled"))
                useBackgroundAgent = Convert.ToBoolean(IsolatedStorageSettings.ApplicationSettings["userData.settings.backgroundagent.enabled"] as String);

            if (useBackgroundAgent)
            {
                //Live Tile
                News lastNews = news;

                FlipTileData flipTileData = new FlipTileData()
                {
                    BackContent = lastNews.title,
                    WideBackContent = lastNews.title,
                    BackBackgroundImage = new Uri("/Assets/Tiles/FlipCycleTileMedium_back.png", UriKind.Relative),
                    WideBackBackgroundImage = new Uri("/Assets/Tiles/FlipCycleTileLarge_back.png", UriKind.Relative),
                    BackTitle = "Пульс"
                };
                

                if (lastNews.title.Length >= 30)
                    flipTileData.BackContent = lastNews.title.Substring(0, lastNews.title.Substring(0, 30).LastIndexOf(' ')) + "...";
                if (lastNews.title.Length >= 75)
                    flipTileData.WideBackContent = lastNews.title.Substring(0, lastNews.title.Substring(0, 75).LastIndexOf(' ')) + "...";

                ShellTile appTile = ShellTile.ActiveTiles.First();
                if (appTile != null)
                {
                    appTile.Update(flipTileData);
                }
            }
            else
            {
                ResetTile();
            }
        }

        public static void ResetTile()
        {
            //Live Tile
            FlipTileData flipTileData = new FlipTileData()
            {
                BackContent = "",
                WideBackContent = "",
                BackBackgroundImage = new Uri("", UriKind.Relative),
                WideBackBackgroundImage = new Uri("", UriKind.Relative),
                BackTitle = ""
            };

            ShellTile appTile = ShellTile.ActiveTiles.First();
            if (appTile != null)
            {
                appTile.Update(flipTileData);
            }
        }

        public static string getDate()
        {
            string date = DateTime.Now.ToString("yyyy.MM.dd HH:mm:ss");
            date = date.Replace(" ", string.Empty);
            date = date.Replace(".", string.Empty);
            date = date.Replace(":", string.Empty);
            date = date.Replace("/", string.Empty);
            date = date.Replace("\"", string.Empty);
            return date;
        }

        public static ScheduleFile RefactorSchedule(ScheduleFile file)
        {
            ScheduleFile data = file;

            List<String[]> daysLang = new List<String[]>();
            daysLang.Add(new String[2] { "ПН", "ПОНЕДЕЛЬНИК" });
            daysLang.Add(new String[2] { "ВТ", "ВТОРНИК" });
            daysLang.Add(new String[2] { "СР", "СРЕДА" });
            daysLang.Add(new String[2] { "ЧТ", "ЧЕТВЕРГ" });
            daysLang.Add(new String[2] { "ПТ", "ПЯТНИЦА" });
            daysLang.Add(new String[2] { "СБ", "СУББОТА" });
            daysLang.Add(new String[2] { "ВС", "ВОСКРЕСЕНЬЕ" });

            foreach (Day day in data.days)
            {
                foreach (String[] strings in daysLang)
                {
                    if (day.name.ToUpper() == strings[0].ToUpper())
                    {
                        //MessageBox.Show(day.name);
                        day.name = strings[1].ToUpper();

                    }
                }
                day.name = day.name.ToLower();
                day.name = day.name[0].ToString().ToUpper() + day.name.Substring(1);


                if (day.subjects.Capacity == 0)
                {
                    day.subjects = new List<Subject>() { };
                    day.subjects.Add(new Subject("1", "", "", "Занятий нет", ""));
                }

                foreach (Subject subj in day.subjects)
                {
                    //if (subj.Name.EndsWith("\r\n"))
                    //MessageBox.Show(subj.Name);
                    //subj.Name = subj.Name.Remove(subj.Name.Length - 2, 1);

                    if (subj.Num.ToString() == "")
                        subj.Num = "0";

                    Regex rgx = new Regex("\\s+");

                    subj.Name = subj.Name.Replace("\n", "");
                    subj.Name = subj.Name.Replace("\r", "");
                    subj.Name = subj.Name.Replace("<br>", "\n");
                    //subj.Name = rgx.Replace(subj.Name, " ");

                    subj.ClassNum = rgx.Replace(subj.ClassNum, " ");
                    subj.ClassNum = subj.ClassNum.Replace("\n", "");
                    subj.ClassNum = subj.ClassNum.Replace("\r", "");
                    subj.ClassNum = subj.ClassNum.Replace("<br>", "\n");
                    subj.ClassNum = subj.ClassNum.Replace(" ", ", ");

                    if (subj.ClassNum.EndsWith(", "))
                        subj.ClassNum = subj.ClassNum.Substring(0, subj.ClassNum.Length - 3);

                    if (subj.ClassNum.StartsWith(", "))
                        subj.ClassNum = subj.ClassNum.Substring(1, subj.ClassNum.Length - 1);

                    // Events
                    if (subj.day == "")
                        subj.day = day.name;
                }
            }

            return data;
        }
    }
}
