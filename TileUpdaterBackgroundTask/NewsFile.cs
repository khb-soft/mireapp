﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TileUpdaterBackgroundTask
{
    class News
    {
        public int newsid { get; set; }
        public string created { get; set; }
        public string createdText { get; set; }
        public int cat { get; set; }
        public string title { get; set; }
        public string catText { get; set; }
        public string text { get; set; }
        public string textText { get; set; }
        public string headingImg { get; set; }
    }

    class NewsFile
    {
        public List<News> content { get; set; }
        public int weeknumber { get; set; }
        public string weeknumbertext { get; set; }
    }
}
