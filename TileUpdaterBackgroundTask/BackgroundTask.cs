﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Windows.ApplicationModel.Background;
using Windows.Data.Xml.Dom;
using Windows.Networking.Connectivity;
using Windows.Security.Cryptography;
using Windows.Security.Cryptography.Core;
using Windows.Storage;
using Windows.Storage.Streams;
using Windows.System.Profile;
using Windows.UI.Notifications;
using Windows.Web.Syndication;

namespace TileUpdaterBackgroundTask
{
    public sealed class BackgroundTask : IBackgroundTask
    {
        public async void Run(IBackgroundTaskInstance taskInstance)
        {
            // Get a deferral, to prevent the task from closing prematurely 
            // while asynchronous code is still running.
            BackgroundTaskDeferral deferral = taskInstance.GetDeferral();

            //Helpers.DisplayTextToast("BackgroundTaskDeferral", "Test");
            if (isOnline())
            {
                string jsonResult = await DownloadFileFromURL("http://app.mirea.ru/news/?list&deviceid=" + getHardwareId());
                if (jsonResult != "")
                {
                    NewsFile newsFile = JsonConvert.DeserializeObject<NewsFile>(jsonResult);
                    UpdateTile(newsFile);
                }
            }

            // Inform the system that the task is finished.
            deferral.Complete();
        }

        private async Task<string> DownloadFileFromURL(string url)
        {
            string result = "";
            try
            {
                var client = new HttpClient(); // Add: using System.Net.Http;
                var response = await client.GetAsync(new Uri(url));
                result = await response.Content.ReadAsStringAsync();
            }
            catch
            { }
            return result;
        }

        private string getHardwareId()
        {
            HardwareToken token = HardwareIdentification.GetPackageSpecificToken(null);
            IBuffer hardwareId = token.Id;

            HashAlgorithmProvider hasher = HashAlgorithmProvider.OpenAlgorithm(HashAlgorithmNames.Md5);
            IBuffer hashed = hasher.HashData(hardwareId);

            string hashedString = CryptographicBuffer.EncodeToHexString(hashed);
            return hashedString;
        }

        private bool isOnline()
        {
            ConnectionProfile connections = NetworkInformation.GetInternetConnectionProfile();
            bool internet = connections != null && connections.GetNetworkConnectivityLevel() == NetworkConnectivityLevel.InternetAccess;
            return internet;
        }

        private static async void UpdateTile(NewsFile feed)
        {
            // Create a tile update manager for the specified syndication feed.
            var updater = TileUpdateManager.CreateTileUpdaterForApplication();
            updater.EnableNotificationQueue(true);
            updater.Clear();

            var item = feed.content.First();

            XmlDocument tileXml = TileUpdateManager.GetTemplateContent(TileTemplateType.TileWide310x150ImageAndText01);

            var title = item.title;
            string titleText = title == null ? String.Empty : title;
            tileXml.GetElementsByTagName("text")[0].InnerText = titleText;

            XmlNodeList imageAttribute = tileXml.GetElementsByTagName("image");
            ((XmlElement)imageAttribute[0]).SetAttribute("src", item.headingImg);

            // Create a new tile notification. 
            updater.Update(new TileNotification(tileXml));

            tileXml = TileUpdateManager.GetTemplateContent(TileTemplateType.TileWide310x150Text04);

            title = item.title;
            titleText = RemoveAllHtmlTag(item.text);
            tileXml.GetElementsByTagName("text")[0].InnerText = titleText;

            // Create a new tile notification. 
            updater.Update(new TileNotification(tileXml));

            string lastNotificationNewsId = await getFileContent("LastNewsId");
            if (lastNotificationNewsId != "")
            {
                int id = int.Parse(lastNotificationNewsId);
                if (id < item.newsid)
                {
                    await setFileContent("LastNewsId", item.newsid.ToString());
                    //DisplayTextToast(titleText, title);
                }
                else
                {
                    //await Helpers.setFileContent("LastNewsId", "80");
                }
            }
            else
            {
                await setFileContent("LastNewsId", item.newsid.ToString());
                //Helpers.DisplayTextToast(titleText, title);
            }
        }

        private static string RemoveAllHtmlTag(string content)
        {
            string result = content;
            result = Regex.Replace(result, "\n", string.Empty);
            result = Regex.Replace(result, ".*?{.*?}", string.Empty);
            result = Regex.Replace(result, "<p>", "\n");
            result = Regex.Replace(result, "<.*?>", string.Empty);

            result = Regex.Replace(result, "&nbsp;", " ");
            result = Regex.Replace(result, "\\s+", " ");

            result = Regex.Replace(result, "^\n", string.Empty);

            result = Regex.Replace(result, "^\\s", string.Empty);
            return result;
        }

        private static async Task<string> getFileContent(string file)
        {
            string data = "";
            try
            {
                StorageFolder local = Windows.Storage.ApplicationData.Current.LocalFolder;

                if (local != null)
                {
                    // Get the DataFolder folder.
                    var dataFolder = await local.GetFolderAsync("Data");

                    // Get the file.
                    var fileinfolder = await dataFolder.OpenStreamForReadAsync(file);

                    // Read the data.
                    using (StreamReader streamReader = new StreamReader(fileinfolder))
                    {
                        data = streamReader.ReadToEnd();
                    }

                }
            }
            catch
            { }
            return data;
        }

        private static async Task setFileContent(string file, string data)
        {
            try
            {
                // Get the text data from the textbox. 
                byte[] fileBytes = System.Text.Encoding.UTF8.GetBytes(data);

                // Get the local folder.
                StorageFolder local = Windows.Storage.ApplicationData.Current.LocalFolder;

                // Create a new folder name DataFolder.
                var dataFolder = await local.CreateFolderAsync("Data", CreationCollisionOption.OpenIfExists);

                // Create a new file named DataFile.txt.
                var fileinfolder = await dataFolder.CreateFileAsync(file, CreationCollisionOption.ReplaceExisting);

                // Write the data from the textbox.
                using (var s = await fileinfolder.OpenStreamForWriteAsync())
                {
                    s.Write(fileBytes, 0, fileBytes.Length);
                }
            }
            catch
            { }
        }
    }
}
