﻿using mireApp.Classes;
using mireApp.Common;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Storage;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// Шаблон всплывающего элемента параметров задокументирован по адресу http://go.microsoft.com/fwlink/?LinkId=273769

namespace mireApp
{
    public sealed partial class AppSettingsFlyout : SettingsFlyout
    {
        List<String[]> facsids = new List<String[]>();
        List<String[]> formsids = new List<String[]>();
        GroupsFile data = new GroupsFile();

        //Settings
        string group = "";
        string facid = "НЕТ";
        string formid = "НЕТ";
        bool hideBlanc = true;
        bool isGroupsLoaded = false;

        public AppSettingsFlyout()
        {
            this.InitializeComponent();
        }

        public void InitLists()
        {
            //факультеты [it, kib, rts, eiu, ele]
            //формы обучения [day, night, branch]

            facsids.Add(new String[2] { "НЕТ", "НЕТ" });
            facsids.Add(new String[2] { "ИТ", "it" });
            facsids.Add(new String[2] { "Кибернетика ", "kib" });
            facsids.Add(new String[2] { "РТС", "rts" });
            facsids.Add(new String[2] { "Экономики и Упр-я", "eiu" });
            facsids.Add(new String[2] { "Электроники ", "ele" });

            formsids.Add(new String[2] { "НЕТ", "НЕТ" });
            formsids.Add(new String[2] { "День", "day" });
            formsids.Add(new String[2] { "Вечер", "night" });
            formsids.Add(new String[2] { "Заочное отделение", "branch" });
        }

        private void SettingsFlyout_Loaded(object sender, RoutedEventArgs e)
        {
            FacListPicker.IsEnabled = false;
            FormListPicker.IsEnabled = false;
            GroupListPicker.IsEnabled = false;
            data.all_groups = new List<Group>() { };

            // Lists
            InitLists();
            FacListPicker.Items.Clear();
            foreach (String[] array in facsids)
            {
                FacListPicker.Items.Add(array[0]);
            }

            FormListPicker.Items.Clear();
            foreach (String[] array in formsids)
            {
                FormListPicker.Items.Add(array[0]);
            }

            GetGroupsList();
        }

        async void GetGroupsList()
        {
            if (ApplicationData.Current.LocalSettings.Values.ContainsKey("Group"))
                group = (String)ApplicationData.Current.LocalSettings.Values["Group"];

            if (ApplicationData.Current.LocalSettings.Values.ContainsKey("groupfac"))
                facid = (String)ApplicationData.Current.LocalSettings.Values["groupfac"];

            if (ApplicationData.Current.LocalSettings.Values.ContainsKey("groupform"))
                formid = (String)ApplicationData.Current.LocalSettings.Values["groupform"];

            if (ApplicationData.Current.LocalSettings.Values.ContainsKey("hideBlanc"))
                hideBlanc = (bool)ApplicationData.Current.LocalSettings.Values["hideBlanc"];

            if (ApplicationData.Current.LocalSettings.Values.ContainsKey("NewsCount"))
                newsCountSlider.Value = (int)ApplicationData.Current.LocalSettings.Values["NewsCount"];

            if (ApplicationData.Current.LocalSettings.Values.ContainsKey("FontSize"))
                fontSizeSlider.Value = (double)((float)ApplicationData.Current.LocalSettings.Values["FontSize"]);

            if (Helpers.isOnline())
            {
                string jsonResult = await Helpers.DownloadFileFromURL("http://app.mirea.ru/timetable/getgroups/?deviceid=" + Helpers.getHardwareId());
                data = JsonConvert.DeserializeObject<GroupsFile>(jsonResult);
                UpdateGroupSelector(data, facid, formid);
            }
            else
            {
                UpdateGroupSelector(data, facid, formid, true);
                //Helpers.ShowMessage("Невозможно получить список групп, т.к. отсутствует интернет-cоединение.", "НАСТРОЙКИ");
                Helpers.DisplayTextToast("Невозможно получить список групп, т.к. отсутствует интернет-cоединение.", "НАСТРОЙКИ");
            }

            HideBlancToggleSwitch.IsOn = hideBlanc;
        }

        public void UpdateGroupSelector(GroupsFile groupsFile, string fac, string form, bool offline = false)
        {
            FacListPicker.IsEnabled = true;

            if (!offline)
            {
                if (facid == "НЕТ")
                {
                    formid = "НЕТ";
                    FormListPicker.IsEnabled = false;
                    group = "НЕТ";
                    GroupListPicker.IsEnabled = false;
                }
                else if (formid == "НЕТ")
                {
                    FormListPicker.IsEnabled = true;
                    GroupListPicker.IsEnabled = false;
                }
                else
                {
                    FormListPicker.IsEnabled = true;
                    GroupListPicker.IsEnabled = true;
                }
            }
            else
            {
                FacListPicker.IsEnabled = false;
                FormListPicker.IsEnabled = false;
                GroupListPicker.IsEnabled = false;
            }

            //groupsFile.all_groups.Insert(0, new Group("НЕТ", 0, "none", "none
            int count = 0;
            foreach (String[] array in facsids)
            {
                if (array[1] == facid)
                    FacListPicker.SelectedIndex = count;
                count++;
            }

            count = 0;
            foreach (String[] array in formsids)
            {
                if (array[1] == formid)
                    FormListPicker.SelectedIndex = count;
                count++;
            }

            GroupListPicker.Items.Clear();

            count = 0;
            foreach (Group grouptoadd in groupsFile.all_groups)
            {
                if (grouptoadd.faculty == fac && grouptoadd.form == form)
                {
                    GroupListPicker.Items.Add(grouptoadd.name);
                    count++;
                    if (grouptoadd.name == group)
                        GroupListPicker.SelectedIndex = count - 1;
                }
            }
            if (count == 0)
                GroupListPicker.Items.Add("НЕТ");

            isGroupsLoaded = true;
            /*if (offline)
            {
                if (group != "НЕТ")
                {
                    GroupListPicker.Items.Add(group);
                    GroupListPicker.SelectedIndex = 1;
                }
                else
                {
                    GroupListPicker.SelectedIndex = 0;
                }
                    
                
            }*/
        }

        private void FacListPicker_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (isGroupsLoaded)
            {
                int id = FacListPicker.SelectedIndex;

                facid = facsids[id][1];

                if (facid == "НЕТ")
                {
                    formid = "НЕТ";
                    FormListPicker.IsEnabled = false;
                    group = "НЕТ";
                    GroupListPicker.IsEnabled = false;
                }
                else
                {
                    FormListPicker.IsEnabled = true;
                }
                UpdateGroupSelector(data, facid, formid);
                SaveCurrentSettings();
            }
        }

        private void FormListPicker_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (isGroupsLoaded)
            {
                int id = FormListPicker.SelectedIndex;

                formid = formsids[id][1];
                if (formid == "НЕТ")
                {
                    group = "НЕТ";
                    GroupListPicker.IsEnabled = false;
                }
                else
                {
                    GroupListPicker.IsEnabled = true;
                }
                UpdateGroupSelector(data, facid, formid);
                SaveCurrentSettings();
            }
        }

        private void GroupListPicker_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (isGroupsLoaded)
            {
                group = GroupListPicker.SelectedItem as String;
                SaveCurrentSettings();
            }
        }

        private void AppBarButton_Click(object sender, RoutedEventArgs e)
        {
            AppBarButton button = (AppBarButton)sender;
            if (button.Name == "ResetAppBarButton")
            {
                SaveCurrentSettings();
            }

            if (button.Name == "SaveAppBarButton")
            {
                ResetSettings();
            }

        }

        public void SaveCurrentSettings()
        {
            ApplicationData.Current.LocalSettings.Values["Group"] = group;
            ApplicationData.Current.LocalSettings.Values["groupfac"] = facid;
            ApplicationData.Current.LocalSettings.Values["groupform"] = formid;
            ApplicationData.Current.LocalSettings.Values["hideBlanc"] = HideBlancToggleSwitch.IsOn;
            ApplicationData.Current.LocalSettings.Values["NewsCount"] = (int)newsCountSlider.Value;
            ApplicationData.Current.LocalSettings.Values["FontSize"] = (float)fontSizeSlider.Value;
        }

        public void ResetSettings()
        {
            ApplicationData.Current.LocalSettings.Values["Group"] = "НЕТ";
            ApplicationData.Current.LocalSettings.Values["groupfac"] = "НЕТ";
            ApplicationData.Current.LocalSettings.Values["groupform"] = "НЕТ";
            ApplicationData.Current.LocalSettings.Values["hideBlanc"] = true;
            ApplicationData.Current.LocalSettings.Values["NewsCount"] = 5;
            ApplicationData.Current.LocalSettings.Values["FontSize"] = 1.0f;
        }

        private void HideBlancToggleSwitch_Toggled(object sender, RoutedEventArgs e)
        {
            if (isGroupsLoaded)
            {
                SaveCurrentSettings();
            }
        }

        private void newsCountSlider_ValueChanged(object sender, RangeBaseValueChangedEventArgs e)
        {
            if (isGroupsLoaded)
            {
                SaveCurrentSettings();
            }
        }
    }
}
