﻿using mireApp.Classes;
using mireApp.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Windows.Input;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;


// Шаблон элемента страницы разделов задокументирован по адресу http://go.microsoft.com/fwlink/?LinkId=234229

namespace mireApp
{
    /// <summary>
    /// Страница, на которой показываются общие сведения об отдельной группе, включая предварительный просмотр элементов
    /// внутри группы.
    /// </summary>
    public sealed partial class NewsSectionPage : Page
    {
        private NavigationHelper navigationHelper;
        private HubLoader loader;

        private NewsFile newsFile;

        public GridView studyGridView;
        public GridView coursesGridView;
        public GridView lifeGridView;

        /// <summary>
        /// NavigationHelper используется на каждой странице для облегчения навигации и 
        /// управление жизненным циклом процесса
        /// </summary>
        public NavigationHelper NavigationHelper
        {
            get { return this.navigationHelper; }
        }


        public NewsSectionPage()
        {
            this.InitializeComponent();
            this.navigationHelper = new NavigationHelper(this);
            this.navigationHelper.LoadState += navigationHelper_LoadState;

            loader = new HubLoader(3);
            loader.LoadCompleated += loader_LoadCompleated;
        }

        public void loader_LoadCompleated(object sender, EventArgs e)
        {
            UpdateNewsList(newsFile);
        }

        private void newsGridView_ItemClick(object sender, ItemClickEventArgs e)
        {
            this.Frame.Navigate(typeof(NewsViewerPage), (News)e.ClickedItem);
        }

        /// <summary>
        /// Заполняет страницу содержимым, передаваемым в процессе навигации.  Также предоставляется любое сохраненное состояние
        /// при повторном создании страницы из предыдущего сеанса.
        /// </summary>
        /// <param name="sender">
        /// Источник события; как правило, <see cref="NavigationHelper"/>
        /// </param>
        /// <param name="e">Данные события, предоставляющие параметр навигации, который передается
        /// <see cref="Frame.Navigate(Type, Object)"/> при первоначальном запросе этой страницы и
        /// словарь состояний, сохраненных этой страницей в ходе предыдущего
        /// сеанса.  Это состояние будет равно NULL при первом посещении страницы.</param>
        private void navigationHelper_LoadState(object sender, LoadStateEventArgs e)
        {
            // TODO: Создание соответствующей модели данных для области проблемы, чтобы заменить пример данных
            /*var group = await SampleDataSource.GetGroupAsync((String)e.NavigationParameter);
            this.DefaultViewModel["Group"] = group;
            this.DefaultViewModel["Items"] = group.Items;*/
            newsFile = (NewsFile)e.NavigationParameter;
        }

        #region Регистрация NavigationHelper

        /// Методы, предоставленные в этом разделе, используются исключительно для того, чтобы
        /// NavigationHelper для отклика на методы навигации страницы.
        /// 
        /// Логика страницы должна быть размещена в обработчиках событий для 
        /// <see cref="GridCS.Common.NavigationHelper.LoadState"/>
        /// и <see cref="GridCS.Common.NavigationHelper.SaveState"/>.
        /// Параметр навигации доступен в методе LoadState 
        /// в дополнение к состоянию страницы, сохраненному в ходе предыдущего сеанса.

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            navigationHelper.OnNavigatedTo(e);
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            navigationHelper.OnNavigatedFrom(e);
        }

        #endregion

        public void UpdateNewsList(NewsFile data)
        {
            studyGridView.Items.Clear();
            coursesGridView.Items.Clear();
            lifeGridView.Items.Clear();

            foreach (News news in data.content)
            {
                switch (news.cat)
                {
                    case 1:
                        studyGridView.Items.Add(news);
                        break;
                    case 2:
                        coursesGridView.Items.Add(news);
                        break;
                    case 3:
                        lifeGridView.Items.Add(news);
                        break;
                }
                    
            }
            //newsGridView.ItemsSource = data.content;
        }

        private void studyGridView_Loaded(object sender, RoutedEventArgs e)
        {
            studyGridView = (GridView)sender;
            loader.ModuleLoaded();
        }

        private void coursesGridView_Loaded(object sender, RoutedEventArgs e)
        {
            coursesGridView = (GridView)sender;
            loader.ModuleLoaded();
        }

        private void lifeGridView_Loaded(object sender, RoutedEventArgs e)
        {
            lifeGridView = (GridView)sender;
            loader.ModuleLoaded();
        }

        private void AppBarButton_Click(object sender, RoutedEventArgs e)
        {
            Frame rootFrame = Window.Current.Content as Frame;
            rootFrame.Navigate(typeof(HubPage));
        }
    }
}