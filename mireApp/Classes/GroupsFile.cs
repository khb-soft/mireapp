﻿using System;
using System.Collections.Generic;
using System.Text;

namespace mireApp.Classes
{
    public class Group
    {
        public string name { get; set; }
        public int course { get; set; }
        public string faculty { get; set; }
        public string form { get; set; }

        public Group(string name, int course, string faculty, string form)
        {
            this.name = name;
            this.course = course;
            this.faculty = faculty;
            this.form = form;
        }
    }

    public class GroupsFile
    {
        public string result { get; set; }
        public List<Group> all_groups { get; set; }
    }
}
