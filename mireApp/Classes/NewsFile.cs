﻿using mireApp.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Windows.Storage.Streams;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media.Imaging;

namespace mireApp.Classes
{
    public class News
    {
        public int newsid { get; set; }
        public DateTime created { get; set; }
        public string createdText { get; set; }
        public int cat { get; set; }
        public string title { get; set; }
        public string catText { get; set; }
        public string text { get; set; }
        public string textText { get; set; }
        public string headingImg { get; set; }

        public News(int newsid, DateTime created, int cat, String title, String text, String headingImg)
        {
            this.newsid = newsid;
            this.created = created;
            this.cat = cat;
            this.title = title;
            this.text = text;

            if (headingImg == string.Empty)
                this.headingImg = "Assets/no_image.png";
            else
                this.headingImg = headingImg;

            switch (cat)
            {
                case 1:
                    this.catText = "Учеба";
                    break;
                case 2:
                    this.catText = "Курсы";
                    break;
                case 3:
                    this.catText = "Жизнь";
                    break;
            }
            this.catText = this.catText.ToUpper();
            createdText = created.ToString("dd.MM");
        }
    }

    public class Banner
    {
        public string bannerid { get; set; }
        public string text { get; set; }
        public string linkedid { get; set; }
    }

    public class NewsFile
    {
        public List<News> content { get; set; }
        public int weeknumber { get; set; }
        public string weeknumbertext { get; set; }
        public List<Banner> banner { get; set; }

        public NewsFile(List<News> content, string weeknumbertext)
        {
            this.content = content;
            this.weeknumbertext = weeknumbertext;
        }
    }
}
