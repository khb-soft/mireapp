﻿using mireApp.Classes;
using mireApp.Common;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Text.RegularExpressions;
using Windows.ApplicationModel.Appointments;
using Windows.ApplicationModel.Background;
using Windows.Data.Xml.Dom;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Storage;
using Windows.UI.ApplicationSettings;
using Windows.UI.Notifications;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// Шаблон элемента страницы концентратора задокументирован по адресу http://go.microsoft.com/fwlink/?LinkID=321224

namespace mireApp
{
    /// <summary>
    /// Страница, на которой отображается сгруппированная коллекция элементов.
    /// </summary>
    public sealed partial class HubPage : Page
    {
        private NavigationHelper navigationHelper;
        private HubLoader loader;

        private GridView newsGridView;
        private GridView banerGridView;
        private GridView scheduleGridView;
        private WebView scherduleWebView;

        public string appDataGroupName;
        public bool appDataHideBlanc;
        public int appDataNewsCount;

        private NewsFile newsFile;
        private ScheduleFile scheduleFile;

        /// <summary>
        /// NavigationHelper используется на каждой странице для облегчения навигации и 
        /// управление жизненным циклом процесса
        /// </summary>
        public NavigationHelper NavigationHelper
        {
            get { return this.navigationHelper; }
        }

        public HubPage()
        {
            this.InitializeComponent();
            this.navigationHelper = new NavigationHelper(this);
            this.navigationHelper.LoadState += navigationHelper_LoadState;

            this.loader = new HubLoader(3);
            this.loader.LoadCompleated += loader_LoadCompleated;
        }

        /// <summary>
        /// Заполняет страницу содержимым, передаваемым в процессе навигации.  Также предоставляется любое сохраненное состояние
        /// при повторном создании страницы из предыдущего сеанса.
        /// </summary>
        /// <param name="sender">
        /// Источник события; как правило, <see cref="NavigationHelper"/>
        /// </param>
        /// <param name="e">Данные события, предоставляющие параметр навигации, который передается
        /// <see cref="Frame.Navigate(Type, Object)"/> при первоначальном запросе этой страницы и
        /// словарь состояний, сохраненных этой страницей в ходе предыдущего
        /// сеанса.  Это состояние будет равно NULL при первом посещении страницы.</param>
        private void navigationHelper_LoadState(object sender, LoadStateEventArgs e)
        {
            // TODO: Создание соответствующей модели данных для области проблемы, чтобы заменить пример данных
            /*var sampleDataGroup = await SampleDataSource.GetGroupAsync("Group-4");
            this.DefaultViewModel["Section3Items"] = sampleDataGroup;*/
        }

        private void GridView_Loaded(object sender, RoutedEventArgs e)
        {
            newsGridView = (GridView)sender;
            /*
            newsGridView.Items.Add(new News(95, DateTime.Now, 3, "«Мы — Команда!». Новости.", "Lal", "Assets/news95.png"));
            newsGridView.Items.Add(new News(94, DateTime.Now.AddDays(-10), 3, "«Мы — команда!» 2015", "Lal", "Assets/news94.png"));
            newsGridView.Items.Add(new News(93, DateTime.Now.AddDays(-14), 3, "[Ступени] Декабрь 2014", "Lal", "Assets/news93.png"));
            newsGridView.Items.Add(new News(92, DateTime.Now.AddDays(-28), 3, "Получение банковских карт", "Lal", "Assets/news92.png"));
            newsGridView.Items.Add(new News(91, DateTime.Now.AddDays(-32), 3, "И снова билеты в театр!", "Lal", "Assets/news91.png"));
            newsGridView.Items.Add(new News(90, DateTime.Now.AddDays(-69), 3, "Билеты на спектакль «Времена не выбирают»", "Lal", "Assets/news90.png"));
            */
            loader.ModuleLoaded();
        }

        private void GridView_Loaded2(object sender, RoutedEventArgs e)
        {
            scheduleGridView = (GridView)sender;
            /*
            ScheduleFile data = new ScheduleFile("ok", new List<Day>());
            data.days.Add(new Day("РАСПИСАНИЕ ЗАНЯТИЙ:", new List<Subject> {
                        new Subject("1", "9:00—10:35", "А-10", "Первая пара", "Перерыв с 10:35 до 10:45"),
                        new Subject("2", "10:45—12:20", "Г-227", "Вторая пара", "Перерыв с 12:20 до 12:50"),
                        new Subject("3", "12:50—14:25", "В-9, Г-128", "Третяя пара", "Перерыв с 14:25 до 14:35"),
                        new Subject("4", "14:35—16:10", "Г-227", "Четвертая пара", "Перерыв с 16:10 до 16:20"),
                        new Subject("5", "16:20—17:55", "Г-227", "Пятая пара", "Перерыв с 17:55 до 18:00"),
                //new ScheduleSubject(6, "18:00—19:35", "", "Шестая пара", ""),
                new Subject("0", "", "", " ", "Вы можете выбрать расписание для Вашей группы в настройках приложения. А еще Вы можете создавать напоминания в календаре, указав нужную пару.")
                    }));
            */
            loader.ModuleLoaded();
        }

        private void GridView_Loaded_1(object sender, RoutedEventArgs e)
        {
            banerGridView = (GridView)sender;
            loader.ModuleLoaded();
        }

        private void WebView_Loaded(object sender, RoutedEventArgs e)
        {
            scherduleWebView = (WebView)sender;
            scherduleWebView.Navigate(new Uri("http://app.mirea.ru/timetable/getschedule/?group=%D0%98%D0%A1%D0%91-1-11&makehtml=true&includeStyles=true&hideBlanc=true"));
            loader.ModuleLoaded();
        }

        /// <summary>
        /// Вызывается при нажатии заголовка HubSection.
        /// </summary>
        /// <param name="sender">Концентратор, который содержит элемент HubSection, заголовок которого щелкнул пользователь.</param>
        /// <param name="e">Данные о событии, описывающие, каким образом было инициировано нажатие.</param>
        void Hub_SectionHeaderClick(object sender, HubSectionHeaderClickEventArgs e)
        {
            HubSection section = e.Section;
            if (section.Name == "newsHubSection")
                this.Frame.Navigate(typeof(NewsSectionPage), newsFile);

            if (section.Name == "scheduleHubSection")
            {
                AppSettingsFlyout CustomSettingFlyout = new AppSettingsFlyout();
                CustomSettingFlyout.ShowIndependent();
                CustomSettingFlyout.Unloaded += CustomSettingFlyout_Unloaded;
            }
        }

        /// <summary>
        /// Вызывается при нажатии элемента внутри раздела.
        /// </summary>
        /// <param name="sender">Представление сетки или списка
        /// в котором отображается нажатый элемент.</param>
        /// <param name="e">Данные о событии, описывающие нажатый элемент.</param>
        void ItemView_ItemClick(object sender, ItemClickEventArgs e)
        {
            // Переход к соответствующей странице назначения и настройка новой страницы
            // путем передачи необходимой информации в виде параметра навигации
            /*var itemId = ((SampleDataItem)e.ClickedItem).UniqueId;*/
            this.Frame.Navigate(typeof(NewsViewerPage), (News)e.ClickedItem);
        }

        private void GridView_ItemClick(object sender, ItemClickEventArgs e)
        {
            this.Frame.Navigate(typeof(NewsViewerPage), (News)e.ClickedItem);
        }

        private async void AppBarButton_Click(object sender, RoutedEventArgs e)
        {
            AppBarButton button = (AppBarButton)sender;
            string help = @"Полезные советы:
Чтобы открыть окно настроек, нажмите на шестеренку в правом верхнем углу экрана или откройте charm-панель (нажмите сочетание клавиш Win+C или наведите курсор мыши на правый угол экрана и проведите по экрану пальцем от края к центру) и выберите настройки.

Вы можете выбрать группу, настроить расписание, изменить размер шрифта и количество новостей на главной странице (в настройках приложения).

Для более удобного чтения новостей мы рекомендуем использовать разделение экрана (наведите курсор на верх экрана (должна появится панель приложения с иконкой и кнопкой закрыть), нажмите на иконку приложения и выберите пункт 'разделить влево/вправо'; есть и другие методы - мы настоятельно рекомендуем Вам ознакомится с инструкцией по Windows 8.1).

Можно создавать напоминания в календаре для пар, у которых указано время. Для этого выберите нужную пару (в расписании) и если Вас устраивает дата и время, то нажмите ОК в открывшемся окне.";
            if (button.Name == "helpAppBarButton")
                await Helpers.ShowMessage("Официальное приложение студентов и аспирантов МГТУ МИРЭА. В нём собраны новости вуза, давая возможность пользователям всегда оставаться в курсе событий.\n\n"+ help +"\n\nРазработчик: KHB-Soft\nПрограммист: Oxy949\nВерсия: " + Helpers.getApplicationVersion() + "\nРазработано совместно с iStudio\nДля связи: oxy949@gmail.com", "ПУЛЬС МИРЭА");
            if (button.Name == "settingsAppBarButton")
            {
                AppSettingsFlyout CustomSettingFlyout = new AppSettingsFlyout();
                CustomSettingFlyout.ShowIndependent();
                CustomSettingFlyout.Unloaded += CustomSettingFlyout_Unloaded;
                //Helpers.ShowMessage("Lol");
            }
            if (button.Name == "refreshAppBarButton")
            {
                Frame rootFrame = Window.Current.Content as Frame;
                rootFrame.Navigate(typeof(HubPage), "refresh");
            }
        }

        private Windows.Foundation.Rect GetElementRect(FrameworkElement element)
        {
            Windows.UI.Xaml.Media.GeneralTransform buttonTransform = element.TransformToVisual(null);
            Windows.Foundation.Point point = buttonTransform.TransformPoint(new Windows.Foundation.Point());
            return new Windows.Foundation.Rect(point, new Windows.Foundation.Size(element.ActualWidth, element.ActualHeight));
        }

        private async void GridView_ItemClick_1(object sender, ItemClickEventArgs e)
        {

            Subject selected = (Subject)e.ClickedItem;
            //MessageBox.Show(selected.day);
            int index = selected.Time.LastIndexOf("—");
            string start = "0";
            string end = "0";
            if (index > 0)
            {
                start = selected.Time.Substring(0, index);
                //MessageBox.Show(start);
                end = selected.Time.Substring(index + 1);
                //MessageBox.Show(end);
            }

            DateTime today = DateTime.Today;
            DayOfWeek dow = DayOfWeek.Monday;
            switch (selected.day.ToUpper())
            {
                case ("ПОНЕДЕЛЬНИК"):
                    dow = DayOfWeek.Monday;
                    break;
                case ("ВТОРНИК"):
                    dow = DayOfWeek.Tuesday;
                    break;
                case ("СРЕДА"):
                    dow = DayOfWeek.Wednesday;
                    break;
                case ("ЧЕТВЕРГ"):
                    dow = DayOfWeek.Thursday;
                    break;
                case ("ПЯТНИЦА"):
                    dow = DayOfWeek.Friday;
                    break;
                case ("СУББОТА"):
                    dow = DayOfWeek.Sunday;
                    break;
                case ("ВОСКРЕСЕНЬЕ"):
                    dow = DayOfWeek.Sunday;
                    break;
                default:
                    dow = today.AddDays(1).DayOfWeek;
                    break;
            }
            int daysUntilDow = ((int)dow - (int)today.DayOfWeek + 7) % 7;
            DateTime nextDow = today.AddDays(daysUntilDow);

            try
            {
                Windows.ApplicationModel.Appointments.Appointment saveAppointmentTask = new Windows.ApplicationModel.Appointments.Appointment();
                saveAppointmentTask.StartTime = DateTime.Parse(start).AddDays(daysUntilDow);
                saveAppointmentTask.Duration = DateTime.Parse(end).AddDays(daysUntilDow) - DateTime.Parse(start).AddDays(daysUntilDow);
                saveAppointmentTask.Subject = "" + selected.Name;
                saveAppointmentTask.Location = "МГТУ МИРЭА, " + selected.ClassNum;
                saveAppointmentTask.Details = "Ведет: " + selected.Professor;
                saveAppointmentTask.AllDay = false;
                saveAppointmentTask.Reminder = TimeSpan.FromMinutes(15);

                String appointmentId = await Windows.ApplicationModel.Appointments.AppointmentManager.ShowAddAppointmentAsync(saveAppointmentTask, GetElementRect((FrameworkElement)sender));
            }
            catch
            { }
        }

        void CustomSettingFlyout_Unloaded(object sender, RoutedEventArgs e)
        {
            Frame rootFrame = Window.Current.Content as Frame;
            rootFrame.Navigate(typeof(HubPage), "refresh");
        }

        public async void loader_LoadCompleated(object sender, EventArgs e)
        {
            //Helpers.ShowMessage("Loaded!");
            loadingProgressBar.Visibility = Visibility.Visible;

            LoadAppSettings();

            if (!navigationHelper.CanGoForward())
            {

                if (Helpers.isOnline())
                {
                    string jsonResult = await Helpers.getFileContent("NewsData");
                    if (jsonResult != "")
                    {
                        newsFile = JsonConvert.DeserializeObject<NewsFile>(jsonResult);
                        UpdateNewsList(newsFile);
                    }
                    else
                    {
                        //await Helpers.ShowMessage("Ошибка при загрузке новостей! Пустой файл.", "ВНИМАНИЕ!!");
                    }

                    //Helpers.ShowMessage(jsonResult2);
                    jsonResult = await Helpers.getFileContent("ScheduleData");
                    if (jsonResult != "")
                    {
                        scheduleFile = JsonConvert.DeserializeObject<ScheduleFile>(jsonResult);
                        UpdateSchedule(scheduleFile);
                    }
                    else
                    {
                        //await Helpers.ShowMessage("Ошибка при загрузке расписания! Пустой файл.", "ВНИМАНИЕ!!");
                    }

                    //News
                    jsonResult = await Helpers.DownloadFileFromURL("http://app.mirea.ru/news/?list&deviceid=" + Helpers.getHardwareId());
                    if (jsonResult != "")
                    {
                        await Helpers.setFileContent("NewsData", jsonResult);
                        newsFile = JsonConvert.DeserializeObject<NewsFile>(jsonResult);
                        UpdateNewsList(newsFile);
                    }
                    else
                    {
                        Helpers.DisplayTextToast("Ошибка при загрузке новостей! Отсутствует подключение к сети интернет.", "ВНИМАНИЕ!!");
                    }

                    //Schedule
                    if (appDataGroupName != "НЕТ")
                    {
                        jsonResult = await Helpers.DownloadFileFromURL("http://app.mirea.ru/timetable/getschedule/?group=" + appDataGroupName + "&hideBlanc=" + appDataHideBlanc.ToString().ToLower() + "&deviceid=" + Helpers.getHardwareId());
                        if (jsonResult != "")
                        {
                            await Helpers.setFileContent("ScheduleData", jsonResult);
                            scheduleFile = JsonConvert.DeserializeObject<ScheduleFile>(jsonResult);
                            UpdateSchedule(scheduleFile);
                        }
                        else
                        {
                            Helpers.DisplayTextToast("Ошибка при загрузке расписания! Отсутствует подключение к сети интернет.", "ВНИМАНИЕ!!");
                        }
                    }
                    else
                    {
                        getEmptySchedule();
                    }

                    loadingProgressBar.Visibility = Visibility.Collapsed;
                }
                else
                {
                    Helpers.DisplayTextToast("Отсутствует подключение к сети интернет.", "ВНИМАНИЕ!!");

                    string jsonResult = await Helpers.getFileContent("NewsData");
                    if (jsonResult != "")
                    {
                        newsFile = JsonConvert.DeserializeObject<NewsFile>(jsonResult);
                        UpdateNewsList(newsFile);
                    }
                    else
                    {
                        //await Helpers.ShowMessage("Ошибка при загрузке новостей! Пустой файл.", "ВНИМАНИЕ!!");
                    }

                    //Helpers.ShowMessage(jsonResult2);
                    jsonResult = await Helpers.getFileContent("ScheduleData");
                    if (jsonResult != "")
                    {
                        scheduleFile = JsonConvert.DeserializeObject<ScheduleFile>(jsonResult);
                        UpdateSchedule(scheduleFile);
                    }
                    else
                    {
                        //await Helpers.ShowMessage("Ошибка при загрузке расписания! Пустой файл.", "ВНИМАНИЕ!!");
                    }

                    loadingProgressBar.Visibility = Visibility.Collapsed;
                }
            }
            else
            {
                string jsonResult = await Helpers.getFileContent("NewsData");
                if (jsonResult != "")
                {
                    newsFile = JsonConvert.DeserializeObject<NewsFile>(jsonResult);
                    UpdateNewsList(newsFile);
                }
                else
                {
                    //await Helpers.ShowMessage("Ошибка при загрузке новостей! Пустой файл.", "ВНИМАНИЕ!!");
                }

                //Helpers.ShowMessage(jsonResult2);
                jsonResult = await Helpers.getFileContent("ScheduleData");
                if (jsonResult != "")
                {
                    scheduleFile = JsonConvert.DeserializeObject<ScheduleFile>(jsonResult);
                    UpdateSchedule(scheduleFile);
                }
                else
                {
                    //await Helpers.ShowMessage("Ошибка при загрузке расписания! Пустой файл.", "ВНИМАНИЕ!!");
                }

                loadingProgressBar.Visibility = Visibility.Collapsed;
            }
            CheckTaskRegistration();
            CheckRateApp();
        }

        private async void CheckRateApp()
        {
            int launchedCount = 1;

            if (!ApplicationData.Current.LocalSettings.Values.ContainsKey("LaunchedCount"))
                ApplicationData.Current.LocalSettings.Values["LaunchedCount"] = launchedCount;

            if (ApplicationData.Current.LocalSettings.Values.ContainsKey("LaunchedCount"))
                launchedCount = (int)ApplicationData.Current.LocalSettings.Values["LaunchedCount"];

            if (launchedCount == 5)
            {
                // Create the message dialog and set its content
                var messageDialog = new MessageDialog("Спасибо за то, что скачали это приложение! Если оно вам понравилась, пожалуйста, оцените его и оставьте отзыв.", "Нам важно Ваше мнение!");

                // Add commands and set their callbacks; both buttons use the same callback function instead of inline event handlers
                messageDialog.Commands.Add(new UICommand(
                    "Оценить",
                    new UICommandInvokedHandler(this.CommandInvokedHandler)));
                messageDialog.Commands.Add(new UICommand(
                    "Отмена",
                    new UICommandInvokedHandler(this.CommandInvokedHandler)));

                // Set the command that will be invoked by default
                messageDialog.DefaultCommandIndex = 0;

                // Set the command to be invoked when escape is pressed
                messageDialog.CancelCommandIndex = 1;

                // Show the message dialog
                await messageDialog.ShowAsync();
            }

            launchedCount++;
            ApplicationData.Current.LocalSettings.Values["LaunchedCount"] = launchedCount;
        }

        private void CommandInvokedHandler(IUICommand command)
        {
            if (command.Label == "Оценить")
                Helpers.RateApp();
        }

        private async void CheckTaskRegistration()
        {
            string taskName = "BackgroundTask";
            string taskEntryPoint = "TileUpdaterBackgroundTask.BackgroundTask";

            try
            {
                var backgroundAccessStatus = await BackgroundExecutionManager.RequestAccessAsync();

                if (backgroundAccessStatus == BackgroundAccessStatus.AllowedMayUseActiveRealTimeConnectivity ||
                    backgroundAccessStatus == BackgroundAccessStatus.AllowedWithAlwaysOnRealTimeConnectivity)
                {
                    foreach (var task in BackgroundTaskRegistration.AllTasks)
                    {
                        if (task.Value.Name == taskName)
                        {
                            task.Value.Unregister(true);
                        }
                    }

                    BackgroundTaskBuilder taskBuilder = new BackgroundTaskBuilder();
                    taskBuilder.Name = taskName;
                    taskBuilder.TaskEntryPoint = taskEntryPoint;
                    taskBuilder.SetTrigger(new TimeTrigger(15, false));
                    var registration = taskBuilder.Register();
                }
            }
            catch
            { }
        }

        private async void getEmptySchedule()
        {
            ScheduleFile data = new ScheduleFile("ok", new List<Day>());
            data.days.Add(new Day("Расписание занятий:", new List<Subject> {
                        new Subject("1", "9:00—10:35", "", "Первая пара", "Перерыв с 10:35 до 10:45"),
                        new Subject("2", "10:45—12:20", "", "Вторая пара", "Перерыв с 12:20 до 12:50"),
                        new Subject("3", "12:50—14:25", "", "Третяя пара", "Перерыв с 14:25 до 14:35"),
                        new Subject("4", "14:35—16:10", "", "Четвертая пара", "Перерыв с 16:10 до 16:20"),
                        new Subject("5", "16:20—17:55", "", "Пятая пара", "Перерыв с 17:55 до 18:00"),
                //new ScheduleSubject(6, "18:00—19:35", "", "Шестая пара", ""),
                new Subject("0", "", "", " ", "Вы можете выбрать расписание для Вашей группы в настройках приложения. А еще Вы можете создавать напоминания в календаре, указав нужную пару.")
                    }));

            string jsonResult = JsonConvert.SerializeObject(data);
            await Helpers.setFileContent("ScheduleData", jsonResult);
            UpdateSchedule(data);
        }

        public void LoadAppSettings()
        {
            if (!ApplicationData.Current.LocalSettings.Values.ContainsKey("Group"))
                ApplicationData.Current.LocalSettings.Values["Group"] = "НЕТ";

            if (!ApplicationData.Current.LocalSettings.Values.ContainsKey("hideBlanc"))
                ApplicationData.Current.LocalSettings.Values["hideBlanc"] = true;

            if (!ApplicationData.Current.LocalSettings.Values.ContainsKey("NewsCount"))
                ApplicationData.Current.LocalSettings.Values["NewsCount"] = 5;

            if (!ApplicationData.Current.LocalSettings.Values.ContainsKey("FontSize"))
                ApplicationData.Current.LocalSettings.Values["FontSize"] = 1.0f;
    

            if (ApplicationData.Current.LocalSettings.Values.ContainsKey("Group"))
                appDataGroupName = (String)ApplicationData.Current.LocalSettings.Values["Group"];

            if (ApplicationData.Current.LocalSettings.Values.ContainsKey("hideBlanc"))
                appDataHideBlanc = (bool)ApplicationData.Current.LocalSettings.Values["hideBlanc"];

            if (ApplicationData.Current.LocalSettings.Values.ContainsKey("NewsCount"))
                appDataNewsCount = (int)ApplicationData.Current.LocalSettings.Values["NewsCount"];
        }

        public void UpdateNewsList(NewsFile data)
        {
            // Привет, Саня!
            //data.content.RemoveAt(0);
            //Helpers.UpdateTile(data.content.First());
            News bnews = data.content[0];
            //await Helpers.ShowMessage(Helpers.FindImageUrl(bnews.text));
            bnews.textText = Helpers.RemoveAllHtmlTag(bnews.text);
            if (bnews.textText.Length >= 500 && bnews.textText.Substring(0, 500).Contains('.'))
                bnews.textText = bnews.textText.Substring(0, bnews.textText.Substring(0, 500).LastIndexOf('.')) + "...";
            banerGridView.Items.Clear();
            banerGridView.Items.Add(bnews);
            //data.content.RemoveAt(0);

            newsGridView.Items.Clear();
            for (int i = 1; i < appDataNewsCount; i++)
                newsGridView.Items.Add(data.content[i]);
            //newsGridView.ItemsSource = data.content;
            UpdateTile(data);
        }

        public void UpdateSchedule(ScheduleFile data)
        {
            if (data.result == "ok")
            {
                data = Helpers.RefactorSchedule(data);

                if (appDataGroupName != "НЕТ")
                    scheduleHubSection.Header = appDataGroupName + ": " + newsFile.weeknumbertext;
                else
                {
                    scheduleHubSection.Header = "Выберите группу";
                    scheduleHubSection.IsHeaderInteractive = true;
                }
                scheduleGridView.ItemsSource = data.days;
            }
        }

        #region Регистрация NavigationHelper

        /// Методы, предоставленные в этом разделе, используются исключительно для того, чтобы
        /// NavigationHelper для отклика на методы навигации страницы.
        /// 
        /// Логика страницы должна быть размещена в обработчиках событий для 
        /// <see cref="GridCS.Common.NavigationHelper.LoadState"/>
        /// и <see cref="GridCS.Common.NavigationHelper.SaveState"/>.
        /// Параметр навигации доступен в методе LoadState 
        /// в дополнение к состоянию страницы, сохраненному в ходе предыдущего сеанса.

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            navigationHelper.OnNavigatedTo(e);
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            navigationHelper.OnNavigatedFrom(e);
        }

        #endregion

        private async void UpdateTile(NewsFile feed)
        {
            // Create a tile update manager for the specified syndication feed.
            var updater = TileUpdateManager.CreateTileUpdaterForApplication();
            updater.EnableNotificationQueue(true);
            updater.Clear();

            var item = feed.content.First();

            XmlDocument tileXml = TileUpdateManager.GetTemplateContent(TileTemplateType.TileWide310x150ImageAndText01);

            var title = item.title;
            string titleText = title == null ? String.Empty : title;
            tileXml.GetElementsByTagName("text")[0].InnerText = titleText;

            XmlNodeList imageAttribute = tileXml.GetElementsByTagName("image");
            ((XmlElement)imageAttribute[0]).SetAttribute("src", item.headingImg);

            // Create a new tile notification. 
            updater.Update(new TileNotification(tileXml));

            tileXml = TileUpdateManager.GetTemplateContent(TileTemplateType.TileWide310x150Text04);

            title = item.title;
            titleText = Helpers.RemoveAllHtmlTag(item.text);
            tileXml.GetElementsByTagName("text")[0].InnerText = titleText;

            // Create a new tile notification. 
            updater.Update(new TileNotification(tileXml));

            string lastNotificationNewsId = await Helpers.getFileContent("LastNewsId");
            if (lastNotificationNewsId != "")
            {
                int id = int.Parse(lastNotificationNewsId);
                if (id < item.newsid)
                {
                    await Helpers.setFileContent("LastNewsId", item.newsid.ToString());
                }
                else
                {
                    //await Helpers.setFileContent("LastNewsId", "80");
                }
            }
            else
            {
                await Helpers.setFileContent("LastNewsId", item.newsid.ToString());
                //Helpers.DisplayTextToast(titleText, title);
            }
        }
    }
}
