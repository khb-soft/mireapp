﻿using mireApp.Classes;
using mireApp.Common.ToastContent;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Windows.Data.Xml.Dom;
using Windows.Networking.Connectivity;
using Windows.Security.Cryptography;
using Windows.Security.Cryptography.Core;
using Windows.Storage;
using Windows.Storage.Streams;
using Windows.System;
using Windows.System.Profile;
using Windows.UI;
using Windows.UI.Notifications;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace mireApp.Common
{
    public static class Helpers
    {
        public static string getApplicationVersion()
        {
            var ver = Windows.ApplicationModel.Package.Current.Id.Version;
            return ver.Major.ToString() + "." + ver.Minor.ToString() + "." + ver.Build.ToString() + "." + ver.Revision.ToString();
        }

        public static async void RateApp()
        {
            await Launcher.LaunchUriAsync(new Uri("ms-windows-store:REVIEW?PFN=64KHB-Soft.54416F87AB3CE_g91bmxwp3e30c"));
        }

        public static bool isOnline()
        {
            ConnectionProfile connections = NetworkInformation.GetInternetConnectionProfile();
            bool internet = connections != null && connections.GetNetworkConnectivityLevel() == NetworkConnectivityLevel.InternetAccess;
            return internet;
        }

        public static void UpdateTile(News news)
        {
            XmlDocument tileXml = TileUpdateManager.GetTemplateContent(TileTemplateType.TileWide310x150Text03);

            XmlNodeList tileTextAttributes = tileXml.GetElementsByTagName("text");
            tileTextAttributes[0].InnerText = news.title;

            XmlDocument squareTileXml = TileUpdateManager.GetTemplateContent(TileTemplateType.TileSquare150x150Text04);
            XmlNodeList squareTileTextAttributes = squareTileXml.GetElementsByTagName("text");
            squareTileTextAttributes[0].AppendChild(squareTileXml.CreateTextNode("" + news.title));
            IXmlNode node = tileXml.ImportNode(squareTileXml.GetElementsByTagName("binding").Item(0), true);
            tileXml.GetElementsByTagName("visual").Item(0).AppendChild(node);

            TileNotification tileNotification = new TileNotification(tileXml);

            //tileNotification.ExpirationTime = DateTimeOffset.UtcNow.AddSeconds(10);

            TileUpdateManager.CreateTileUpdaterForApplication().Update(tileNotification);
        }

        public static string getHardwareId()
        {
            HardwareToken token = HardwareIdentification.GetPackageSpecificToken(null);
            IBuffer hardwareId = token.Id;

            HashAlgorithmProvider hasher = HashAlgorithmProvider.OpenAlgorithm(HashAlgorithmNames.Md5);
            IBuffer hashed = hasher.HashData(hardwareId);

            string hashedString = CryptographicBuffer.EncodeToHexString(hashed);
            return hashedString;
        }

        public static void DisplayTextToast(string message)
        {
            // Creates a toast using the notification object model, which is another project
            // in this solution.  For an example using Xml manipulation, see the function
            // DisplayToastUsingXmlManipulation below.
            IToastNotificationContent toastContent = null;

            IToastText01 templateContent = ToastContentFactory.CreateToastText01();
            templateContent.TextBodyWrap.Text = message;
            toastContent = templateContent;

            // Create a toast, then create a ToastNotifier object to show
            // the toast
            ToastNotification toast = toastContent.CreateNotification();

            // If you have other applications in your package, you can specify the AppId of
            // the app to create a ToastNotifier for that application
            ToastNotificationManager.CreateToastNotifier().Show(toast);
        }

        public static void DisplayTextToast(string message, string header)
        {
            // Creates a toast using the notification object model, which is another project
            // in this solution.  For an example using Xml manipulation, see the function
            // DisplayToastUsingXmlManipulation below.
            IToastNotificationContent toastContent = null;

            IToastText02 templateContent = ToastContentFactory.CreateToastText02();
            templateContent.TextHeading.Text = header;
            templateContent.TextBodyWrap.Text = message;
            toastContent = templateContent;


            // Create a toast, then create a ToastNotifier object to show
            // the toast
            ToastNotification toast = toastContent.CreateNotification();

            // If you have other applications in your package, you can specify the AppId of
            // the app to create a ToastNotifier for that application
            ToastNotificationManager.CreateToastNotifier().Show(toast);
        }

        public static String OptimizeHTMLForWP(string html, float fontSize)
        {
            /*
            Color currentPhoneAccentColorHex = Color.FromArgb(255, 0, 0, 0);
            Color currentPhoneBackgroundColorHex = Color.FromArgb(255,0,0,0);
            Color currentPhoneForegroundColorHex = Color.FromArgb(255, 255, 255, 255);
            */
            // Script that will call raise the ScriptNotify via window.external.Notify

            // Inject the Javascript into the head section of the HTML document
            //html = html + notifyJS;
            /*
            string optHtml;
            if (!fullscreen)*/
            float size = fontSize;

            html = "<meta name='viewport' content='width=600, user-scalable=yes' /><style> .PhoneAccentColor { color: #0071BC; }  .PhoneBackgroundColor { color: #FFFFFF; }  .PhonForegroundColor { color: #000000; } body { font-size: " + size.ToString().Replace(',', '.') + "em; background-color: #FFFFFF; font-family: Segoe WP, segoe; color: #000000; margin: 0;} a { color: color: #0071BC;; text-decoration: underline; }</style> " + html;
            //ShowMessage(html);
            /*else
                optHtml = "<meta name='viewport' content='width=480, user-scalable=yes' /><style> .PhoneAccentColor { color: " + ColorToHTML(currentPhoneAccentColorHex) + "; }  .PhoneBackgroundColor { color: " + ColorToHTML(currentPhoneBackgroundColorHex) + "; }  .PhonForegroundColor { color: " + ColorToHTML(currentPhoneForegroundColorHex) + "; } body { font-size: 1.2em; background-color: " + ColorToHTML(currentPhoneBackgroundColorHex) + "; font-family: Segoe WP, segoe; color: " + ColorToHTML(currentPhoneForegroundColorHex) + "; } a { color: " + ColorToHTML(currentPhoneAccentColorHex) + "; text-decoration: underline; }</style> " + html;
            return optHtml;
            */
            return html;
        }

        public static string RemoveAllHtmlTag(string content)
        {
            string result = content;
            result = Regex.Replace(result, "\n", string.Empty);
            result = Regex.Replace(result, ".*?{.*?}", string.Empty);
            result = Regex.Replace(result, "<p>", "\n");
            result = Regex.Replace(result, "<.*?>", string.Empty);
            
            result = Regex.Replace(result, "&nbsp;", " ");
            result = Regex.Replace(result, "\\s+", " ");

            result = Regex.Replace(result, "^\n", string.Empty);

            result = Regex.Replace(result, "^\\s", string.Empty);
            return result;
        }

        public static string FindImageUrl(string content)
        {
            string result = content;
            if (Regex.Matches(content, "<img.+?src=[\"'](.+?)[\"'].*?>").Count > 0)
                result = Regex.Matches(content, "<img.+?src=[\"'](.+?)[\"'].*?>")[0].Groups[1].Value;
            else
                result = "Assets/no_image.png";
            return result;
        }

        public static String ColorToHTML(Color color)
        {
            string html = color.ToString();
            html = html.Remove(1, 2);
            //MessageBox.Show(html);
            return html;
        }

        public static string getDate()
        {
            string date = DateTime.Now.ToString("yyyy.MM.dd HH:mm:ss");
            date = date.Replace(" ", string.Empty);
            date = date.Replace(".", string.Empty);
            date = date.Replace(":", string.Empty);
            date = date.Replace("/", string.Empty);
            date = date.Replace("\"", string.Empty);
            return date;
        }

        public static async Task ShowMessage(string text)
        {
            var dialog = new MessageDialog(text);
            await dialog.ShowAsync();
        }

        public static async Task ShowMessage(string text, string title)
        {
            var dialog = new MessageDialog(text, title);
            await dialog.ShowAsync();
        }

        public static async Task<string> getFileContent(string file)
        {
            string data = "";
            try
            {
                StorageFolder local = Windows.Storage.ApplicationData.Current.LocalFolder;
                
                if (local != null)
                {
                    // Get the DataFolder folder.
                    var dataFolder = await local.GetFolderAsync("Data");

                    // Get the file.
                    var fileinfolder = await dataFolder.OpenStreamForReadAsync(file);

                    // Read the data.
                    using (StreamReader streamReader = new StreamReader(fileinfolder))
                    {
                        data = streamReader.ReadToEnd();
                    }

                }
            }
            catch
            { }
            return data;
        }

        public static async Task setFileContent(string file, string data)
        {
            try
            {
                // Get the text data from the textbox. 
                byte[] fileBytes = System.Text.Encoding.UTF8.GetBytes(data);

                // Get the local folder.
                StorageFolder local = Windows.Storage.ApplicationData.Current.LocalFolder;

                // Create a new folder name DataFolder.
                var dataFolder = await local.CreateFolderAsync("Data", CreationCollisionOption.OpenIfExists);

                // Create a new file named DataFile.txt.
                var fileinfolder = await dataFolder.CreateFileAsync(file, CreationCollisionOption.ReplaceExisting);

                // Write the data from the textbox.
                using (var s = await fileinfolder.OpenStreamForWriteAsync())
                {
                    s.Write(fileBytes, 0, fileBytes.Length);
                }
            }
            catch
            { }
        }

        public static async Task<string> DownloadFileFromURL(string url)
        {
            string result = "";
            try
            {
                var client = new HttpClient(); // Add: using System.Net.Http;
                var response = await client.GetAsync(new Uri(url));
                result = await response.Content.ReadAsStringAsync();
            }
            catch
            { }
            return result;
        }

        public static ScheduleFile RefactorSchedule(ScheduleFile file)
        {
            ScheduleFile data = file;

            List<String[]> daysLang = new List<String[]>();
            daysLang.Add(new String[2] { "ПН", "ПОНЕДЕЛЬНИК" });
            daysLang.Add(new String[2] { "ВТ", "ВТОРНИК" });
            daysLang.Add(new String[2] { "СР", "СРЕДА" });
            daysLang.Add(new String[2] { "ЧТ", "ЧЕТВЕРГ" });
            daysLang.Add(new String[2] { "ПТ", "ПЯТНИЦА" });
            daysLang.Add(new String[2] { "СБ", "СУББОТА" });
            daysLang.Add(new String[2] { "ВС", "ВОСКРЕСЕНЬЕ" });

            foreach (Day day in data.days)
            {
                foreach (String[] strings in daysLang)
                {
                    if (day.name.ToUpper() == strings[0].ToUpper())
                    {
                        //MessageBox.Show(day.name);
                        day.name = strings[1].ToUpper();

                    }
                }
                day.name = day.name.ToLower();
                day.name = day.name[0].ToString().ToUpper() + day.name.Substring(1);


                if (day.subjects.Capacity == 0)
                {
                    day.subjects = new List<Subject>() { };
                    day.subjects.Add(new Subject("1", "", "", "Занятий нет", ""));
                }

                foreach (Subject subj in day.subjects)
                {
                    //if (subj.Name.EndsWith("\r\n"))
                    //MessageBox.Show(subj.Name);
                    //subj.Name = subj.Name.Remove(subj.Name.Length - 2, 1);

                    if (subj.Num.ToString() == "")
                        subj.Num = "0";

                    Regex rgx = new Regex("\\s+");

                    subj.Name = subj.Name.Replace("\n", "");
                    subj.Name = subj.Name.Replace("\r", "");
                    subj.Name = subj.Name.Replace("<br>", "\n");
                    //subj.Name = rgx.Replace(subj.Name, " ");

                    subj.ClassNum = rgx.Replace(subj.ClassNum, " ");
                    subj.ClassNum = subj.ClassNum.Replace("\n", "");
                    subj.ClassNum = subj.ClassNum.Replace("\r", "");
                    subj.ClassNum = subj.ClassNum.Replace("<br>", "\n");
                    subj.ClassNum = subj.ClassNum.Replace(" ", ", ");

                    if (subj.ClassNum.EndsWith(", "))
                        subj.ClassNum = subj.ClassNum.Substring(0, subj.ClassNum.Length - 3);

                    if (subj.ClassNum.StartsWith(", "))
                        subj.ClassNum = subj.ClassNum.Substring(1, subj.ClassNum.Length - 1);

                    // Events
                    if (subj.day == "")
                        subj.day = day.name;
                }
            }

            return data;
        }
    }
}
