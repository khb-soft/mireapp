﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mireApp.Common
{
    public class HubLoader
    {
        int loadedModules = 0;
        int totalModules = 0;
        private bool isAllLoaded = false;

        public bool isLoaded()
        {
            return isAllLoaded;
        }

        public HubLoader(int modules)
        {
            totalModules = modules;
        }

        public void ModuleLoaded()
        {
            loadedModules++;
            if (loadedModules >= totalModules && !isAllLoaded)
            {
                OnLoadCompleated();
                isAllLoaded = true;
            }
        }

        public void ForceLoaded()
        {
            isAllLoaded = true;
        }

        public event EventHandler LoadCompleated;

        public void OnLoadCompleated()
        {
            EventHandler handler = LoadCompleated;
            if (null != handler) handler(this, EventArgs.Empty);
        }

        public void ResetCount()
        {
            loadedModules = 0;
            isAllLoaded = false;
        }
    }
}
