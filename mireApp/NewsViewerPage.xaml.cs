﻿using mireApp.Classes;
using mireApp.Common;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Windows.Input;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Storage;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;


// Шаблон элемента страницы элементов задокументирован по адресу http://go.microsoft.com/fwlink/?LinkId=234232

namespace mireApp
{
    /// <summary>
    /// Страница, на которой отображаются сведения об отдельном элементе внутри группы.
    /// </summary>
    public sealed partial class NewsViewerPage : Page
    {
        private NavigationHelper navigationHelper;
        private HubLoader loader;

        private NewsFile newsFile;
        private News currentNews;

        public GridView lastnewsGridView;

        private bool isLoaded = false;
        private float fontSize = 1.0f;

        /// <summary>
        /// NavigationHelper используется на каждой странице для облегчения навигации и 
        /// управление жизненным циклом процесса
        /// </summary>
        public NavigationHelper NavigationHelper
        {
            get { return this.navigationHelper; }
        }

        public NewsViewerPage()
        {
            this.InitializeComponent();
            this.navigationHelper = new NavigationHelper(this);
            this.navigationHelper.LoadState += navigationHelper_LoadState;

            loader = new HubLoader(1);
            loader.LoadCompleated += loader_LoadCompleated;
        }

        public async void loader_LoadCompleated(object sender, EventArgs e)
        {
            string jsonResult = await Helpers.getFileContent("NewsData");
            if (jsonResult != "")
            {
                newsFile = JsonConvert.DeserializeObject<NewsFile>(jsonResult);
                UpdateNewsList(newsFile);
            }
            else
            {
                //await Helpers.ShowMessage("Ошибка при загрузке новостей! Пустой файл.", "ВНИМАНИЕ!!");
            }

            UpdateNewsList(newsFile);
        }

        private void newsGridView_ItemClick(object sender, ItemClickEventArgs e)
        {
            this.Frame.Navigate(typeof(NewsViewerPage), (News)e.ClickedItem);
        }

        public void UpdateNewsList(NewsFile data)
        {
            lastnewsGridView.Items.Clear();

            int count = 0;
            foreach (News news in data.content)
            {
                if (news.newsid != currentNews.newsid && count < 6)
                {
                    lastnewsGridView.Items.Add(news);
                    count++;
                }
            }
            //newsGridView.ItemsSource = data.content;
        }

        /// <summary>
        /// Заполняет страницу содержимым, передаваемым в процессе навигации.  Также предоставляется любое сохраненное состояние
        /// при повторном создании страницы из предыдущего сеанса.
        /// </summary>
        /// <param name="sender">
        /// Источник события; как правило, <see cref="NavigationHelper"/>
        /// </param>
        /// <param name="e">Данные события, предоставляющие параметр навигации, который передается
        /// <see cref="Frame.Navigate(Type, Object)"/> при первоначальном запросе этой страницы и
        /// словарь состояний, сохраненных этой страницей в ходе предыдущего
        /// сеанса.  Это состояние будет равно NULL при первом посещении страницы.</param>
        private void navigationHelper_LoadState(object sender, LoadStateEventArgs e)
        {
            // TODO: Создание соответствующей модели данных для области проблемы, чтобы заменить пример данных
            try
            {
                currentNews = (News)e.NavigationParameter;
                pageTitle.Text = currentNews.title;

                newsWebView.NavigationCompleted += newsWebView_NavigationCompleted;
                newsWebView.NavigationStarting += newsWebView_NavigationStarting;
                newsWebView.NavigateToString(Helpers.OptimizeHTMLForWP(currentNews.text, fontSize));
            }
            catch
            { }
        }

        private async void newsWebView_NavigationStarting(WebView sender, WebViewNavigationStartingEventArgs args)
        {
            if (isLoaded)
            {
                try
                {
                    await Windows.System.Launcher.LaunchUriAsync(args.Uri);
                }
                catch
                {

                }
                sender.Stop();
            }
        }

        private void lastnewsGridView_Loaded(object sender, RoutedEventArgs e)
        {
            lastnewsGridView = (GridView)sender;
            loader.ModuleLoaded();
        }

        void newsWebView_NavigationCompleted(WebView sender, WebViewNavigationCompletedEventArgs args)
        {
            isLoaded = true;
        }

        #region Регистрация NavigationHelper

        /// Методы, предоставленные в этом разделе, используются исключительно для того, чтобы
        /// NavigationHelper для отклика на методы навигации страницы.
        /// 
        /// Логика страницы должна быть размещена в обработчиках событий для 
        /// <see cref="GridCS.Common.NavigationHelper.LoadState"/>
        /// и <see cref="GridCS.Common.NavigationHelper.SaveState"/>.
        /// Параметр навигации доступен в методе LoadState 
        /// в дополнение к состоянию страницы, сохраненному в ходе предыдущего сеанса.


        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            if (ApplicationData.Current.LocalSettings.Values.ContainsKey("FontSize"))
                fontSize = (float)ApplicationData.Current.LocalSettings.Values["FontSize"];
            navigationHelper.OnNavigatedTo(e);
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            navigationHelper.OnNavigatedFrom(e);
            isLoaded = false;
        }

        #endregion

        private void AppBarButton_Click(object sender, RoutedEventArgs e)
        {
            Frame rootFrame = Window.Current.Content as Frame;
            rootFrame.Navigate(typeof(HubPage));
        }
    }
}