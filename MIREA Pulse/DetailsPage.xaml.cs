﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Resources;
using Resources.Resources;
using System.Xml.Linq;
using System.Collections.ObjectModel;
using System.IO.IsolatedStorage;
using System.Globalization;
using Newtonsoft.Json;
using Microsoft.Phone.Tasks;
using System.Windows.Media.Imaging;

namespace MIREA_Pulse
{
	public partial class DetailsPage : PhoneApplicationPage
	{
		int localnewsid = 0;
        bool useOffline = true;
		string tempPageContent = "";
		string lasturl = "";
        string currenturl = "";
		private Stack<Uri> NavigationStack = new Stack<Uri>();
		Random rnd = new Random();
		List<News> NewsList = new List<News>();
		WebClient NewsDownloader = new WebClient();

		public DetailsPage()
		{
			InitializeComponent();
			NewsDownloader.DownloadStringCompleted += new DownloadStringCompletedEventHandler(NewsDownloadCompleated);
            NewsBrowser.ScriptNotify += browser_ScriptNotify;
		}

        void browser_ScriptNotify(object sender, NotifyEventArgs e)
        {
            if (!string.IsNullOrEmpty(e.Value))
            {
                string href = e.Value.ToLower();
                if (href.StartsWith("mailto:"))
                {
                    EmailComposeTask email = new EmailComposeTask();
                    email.To = href.Replace("mailto:", string.Empty);
                    email.Show();
                }
                else if (href.StartsWith("tel:"))
                {
                    PhoneCallTask call = new PhoneCallTask();
                    call.PhoneNumber = href.Replace("tel:", string.Empty);
                    call.Show();
                }
            }
        }

		protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
		{
			base.OnNavigatedTo(e);

			localnewsid = 0;
			tempPageContent = "";
			NewsBrowser.Opacity = 0;
			TitleTextBlock.Text = "";
			TitleTextBlock.Opacity = 0;
			NavigationStack = new Stack<Uri>();

			string selectedItem = "";
			if (NavigationContext.QueryString.TryGetValue("selectedItem", out selectedItem))
				localnewsid = Convert.ToInt32(selectedItem);
			getNews();
		}

		protected override void OnBackKeyPress(System.ComponentModel.CancelEventArgs e)
		{
			if (NavigationStack.Count > 2)
			{
				//if (NavigationStack.Count != 2)
				// get rid of the topmost item...
				NavigationStack.Pop();
				// now navigate to the next topmost item
				// note that this is another Pop - as when the navigate occurs a Push() will happen
				NewsBrowser.Navigate(NavigationStack.Pop());
				e.Cancel = true;
				return;
			}
			else if (NavigationStack.Count == 2)
			{
				NavigationStack.Pop();
				NewsBrowser.NavigateToString(tempPageContent);
				NavigationStack.Pop();
				e.Cancel = true;
				return;
			}

			base.OnBackKeyPress(e);
		}

		private void getNews()
		{
            if (Helpers.CheckNetworkConnection())
            {
                try
                {
                    StartNewsLoading.Begin();
                    if (!useOffline)
                    {
                        Uri uri = new Uri(AppResources.NewsUrl + "&date=" + Helpers.getDate(), UriKind.Absolute);
                        NewsDownloader.DownloadStringAsync(uri);
                    }
                    else
                    {
                        RefreshNews(Helpers.LoadFile(AppResources.OfflineNewsDBFile));
                    }
                    
                }
                catch
                { }
            }
            else
            {
                if (!useOffline)
                {
                    StartNewsLoading.Begin();
                    NewsBrowser.NavigateToString(Helpers.OptimizeHTMLForWP("Ошибка при загрузке новостей. Проверьте интернет-соединение.", true));
                }
                else
                {
                    // Load offline DB
                    //Helpers.SaveFile("test.json", "hello world from textfile");
                    StartNewsLoading.Begin();
                    RefreshNews(Helpers.LoadFile(AppResources.OfflineNewsDBFile));
                }
            }
		}

		private void NewsDownloadCompleated(object sender, System.Net.DownloadStringCompletedEventArgs e)
		{
			try
			{
				if (e.Result == null || e.Error != null)
				{
					NewsBrowser.NavigateToString(Helpers.OptimizeHTMLForWP("Ошибка при загрузке новостей. Проблемы на сервере.", true));
				}
				else
				{
					RefreshNews(e.Result);
				}
			}
			catch
			{
				NewsBrowser.NavigateToString(Helpers.OptimizeHTMLForWP("Ошибка при загрузке новостей. Проверьте интернет-соединение.", true));
			}
		}

		private void RefreshNews(String jsonResult)
		{
			NewsList.Clear();
			NewsFile data = JsonConvert.DeserializeObject<NewsFile>(jsonResult);
			foreach (News news in data.content)
			{
				if (news.newsid == localnewsid)
				{
                    BitmapImage bitmapImage = new BitmapImage();
                    // Call BaseUri on the root Page element and combine it with a relative path
                    // to consruct an absolute URI.
                    bitmapImage.UriSource = new Uri(news.headingImg, UriKind.RelativeOrAbsolute);
                    IconImage.Source = bitmapImage;
                    if (news.title.Length < 35)
						TitleTextBlock.Text = news.title.ToUpper();
					else
					{
						TitleTextBlock.Text = news.title.ToUpper().Substring(0, news.title.ToUpper().Substring(0, 35).LastIndexOf(' ')) + "...";
					}
						
					NewsBrowser.NavigateToString(Helpers.OptimizeHTMLForWP(news.text, true));
                    tempPageContent = Helpers.OptimizeHTMLForWP(news.text, true);
				}
			}
            //BrowserButton.IsEnabled = false;
            StopNewsLoading.Begin();
		}

		private void NewsBrowser_Navigated(object sender, NavigationEventArgs e)
		{

			NavigationStack.Push(e.Uri);
			lasturl = e.Uri.ToString();
			StopNewsLoading.Begin();

		}

		private void CloseButton_Click(object sender, EventArgs e)
		{
			NavigationService.GoBack();
		}

        private void BrowserButton_Click(object sender, EventArgs e)
        {
            if (NavigationStack.Count > 1)
            {
                Uri uri = new Uri(currenturl);
                WebBrowserTask webBrowserTask = new WebBrowserTask();
                webBrowserTask.Uri = uri;
                webBrowserTask.Show();
            }
            else
            {
                MessageBox.Show("Данную страницу невозможно открыть в браузере.", "ПРОСМОТР В БРАУЗЕРЕ", MessageBoxButton.OK);
            }

            
        }

        private void NewsBrowser_Navigating(object sender, NavigatingEventArgs e)
        {
            currenturl = e.Uri.ToString();
            if (NavigationStack.Count >= 1)
            {
                Uri uri = new Uri(currenturl);
                WebBrowserTask webBrowserTask = new WebBrowserTask();
                webBrowserTask.Uri = uri;
                webBrowserTask.Show();
            }
        }
	}
}