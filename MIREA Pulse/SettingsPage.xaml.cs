﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.IO.IsolatedStorage;
using Resources;
using Resources.Resources;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Telerik.Windows.Controls;

namespace MIREA_Pulse
{
	public partial class SettingsPage : PhoneApplicationPage
	{
		bool wasSettingsChanged = false;
        WebClient GroupsDownloader = new WebClient();
        GroupsFile data = new GroupsFile();
        List<String[]> facsids = new List<String[]>();
        List<String[]> formsids = new List<String[]>();

		//Magic =^_^=
		bool isLoadCompleated = false;
		bool isListsLoaded = false;
        bool isGroupsLoaded = false;
        string group = "";
        string facid = "НЕТ";
        string formid = "НЕТ";

		public SettingsPage()
		{
			InitializeComponent();
            FacListPicker.IsEnabled = false;
            FormListPicker.IsEnabled = false;
            GroupListPicker.IsEnabled = false;
            data.all_groups = new List<Group>() { };
            GroupsDownloader.DownloadStringCompleted += new DownloadStringCompletedEventHandler(GroupsDownloadCompleated);
            
            // Lists
            InitLists();
            FacListPicker.Items.Clear();
            foreach (String[] array in facsids)
            {
                FacListPicker.Items.Add(array[0]);
            }

            FormListPicker.Items.Clear();
            foreach (String[] array in formsids)
            {
                FormListPicker.Items.Add(array[0]);
            }
		}

        public void InitLists()
        {
            //факультеты [it, kib, rts, eiu, ele]
            //формы обучения [day, night, branch]

            facsids.Add(new String[2] { "НЕТ", "НЕТ" });
            facsids.Add(new String[2] { "ИТ", "it" });
            facsids.Add(new String[2] { "Кибернетика ", "kib" });
            facsids.Add(new String[2] { "РТС", "rts" });
            facsids.Add(new String[2] { "Экономики и Упр-я", "eiu" });
            facsids.Add(new String[2] { "Электроники ", "ele" });

            formsids.Add(new String[2] { "НЕТ", "НЕТ" });
            formsids.Add(new String[2] { "День", "day" });
            formsids.Add(new String[2] { "Вечер", "night" });
            formsids.Add(new String[2] { "Заочное отделение", "branch" });
        }

		protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
		{
			base.OnNavigatedTo(e);
			wasSettingsChanged = false;
			isListsLoaded = false;
			LoadSettings();
            GetGroupsList();
		}

		protected override void OnBackKeyPress(System.ComponentModel.CancelEventArgs e)
		{
			if (wasSettingsChanged)
			{
                MessageBoxResult result = MessageBox.Show("Сохранить изменённые настройки приложения", "НАСТРОЙКИ", MessageBoxButton.OKCancel);

				if (result == MessageBoxResult.OK)
				{
					SaveSettings();
				}
			}
			base.OnBackKeyPress(e);
		}

        void GetGroupsList()
        {
            if (Helpers.CheckNetworkConnection())
            {
                Uri uri = new Uri(AppResources.SheduleListUrl + "?date=" + Helpers.getDate() + "&deviceid=" + Helpers.getDeviceId(), UriKind.Absolute);
                GroupsDownloader.DownloadStringAsync(uri);
            }
            else
            {
                MessageBox.Show("Невозможно получить список групп, т.к. отсутствует интернет-cоединение.", "НАСТРОЙКИ", MessageBoxButton.OK);
                UpdateGroupSelector(data, facid, formid, true);
                isGroupsLoaded = true;
            }
        }

        private void GroupsDownloadCompleated(object sender, DownloadStringCompletedEventArgs e)
        {
            try
            {
                if (e.Result == null || e.Error != null)
                {
                    MessageBox.Show("Ошибка при загрузке списка групп. Проблемы на сервере.", "НАСТРОЙКИ", MessageBoxButton.OK);
                }
                else
                {
                    RefreshGroups(e.Result);
                }
            }
            catch
            {
                MessageBox.Show("Ошибка при загрузке списка групп. Проверьте интернет-соединение.", "НАСТРОЙКИ", MessageBoxButton.OK);
            }
        }

        private void RefreshGroups(string jsonResult)
        {
            if (String.IsNullOrEmpty(jsonResult) || jsonResult.Contains("error"))
            {
                MessageBox.Show("Ошибка при чтении списка групп. Проверьте интернет-соединение.", "НАСТРОЙКИ", MessageBoxButton.OK);
                return;
            }

            data = JsonConvert.DeserializeObject<GroupsFile>(jsonResult);
            UpdateGroupSelector(data, facid, formid);

            isGroupsLoaded = true;
        }


        public void UpdateGroupSelector(GroupsFile groupsFile, string fac, string form, bool offline = false)
        {
            FacListPicker.IsEnabled = true;

            if (!offline)
            {
                if (facid == "НЕТ")
                {
                    formid = "НЕТ";
                    FormListPicker.IsEnabled = false;
                    group = "НЕТ";
                    GroupListPicker.IsEnabled = false;
                }
                else if (formid == "НЕТ")
                {
                    FormListPicker.IsEnabled = true;
                    GroupListPicker.IsEnabled = false;
                }
                else
                {
                    FormListPicker.IsEnabled = true;
                    GroupListPicker.IsEnabled = true;
                }
            }
            else
            {
                FacListPicker.IsEnabled = false;
                FormListPicker.IsEnabled = false;
                GroupListPicker.IsEnabled = false;
            }

            //groupsFile.all_groups.Insert(0, new Group("НЕТ", 0, "none", "none
            int count = 0;
            foreach (String[] array in facsids)
            {
                if (array[1] == facid)
                    FacListPicker.SelectedIndex = count;
                count++;
            }

            count = 0;
            foreach (String[] array in formsids)
            {
                if (array[1] == formid)
                    FormListPicker.SelectedIndex = count;
                count++;
            }

            GroupListPicker.Items.Clear();

            count = 0;
            foreach (Group grouptoadd in groupsFile.all_groups)
            {
                if (grouptoadd.faculty == fac && grouptoadd.form == form)
                {
                    GroupListPicker.Items.Add(grouptoadd.name);
                    count++;
                    if (grouptoadd.name == group)
                        GroupListPicker.SelectedIndex = count - 1;
                }
            }
            if (count == 0)
                GroupListPicker.Items.Add("НЕТ");
            /*if (offline)
            {
                if (group != "НЕТ")
                {
                    GroupListPicker.Items.Add(group);
                    GroupListPicker.SelectedIndex = 1;
                }
                else
                {
                    GroupListPicker.SelectedIndex = 0;
                }
                    
                
            }*/
        }

		void LoadSettings()
		{
			isLoadCompleated = false;
            /*
			if (IsolatedStorageSettings.ApplicationSettings.Contains("userData.settings.news.study"))
				StudyCheckBox.IsChecked = Convert.ToBoolean(IsolatedStorageSettings.ApplicationSettings["userData.settings.news.study"] as String);
			if (IsolatedStorageSettings.ApplicationSettings.Contains("userData.settings.news.courses"))
				CoursesCheckBox.IsChecked = Convert.ToBoolean(IsolatedStorageSettings.ApplicationSettings["userData.settings.news.courses"] as String);
			if (IsolatedStorageSettings.ApplicationSettings.Contains("userData.settings.news.live"))
				LiveCheckBox.IsChecked = Convert.ToBoolean(IsolatedStorageSettings.ApplicationSettings["userData.settings.news.live"] as String);
            */
            if (IsolatedStorageSettings.ApplicationSettings.Contains("userData.settings.schedule.group"))
                group = IsolatedStorageSettings.ApplicationSettings["userData.settings.schedule.group"] as String;

            if (IsolatedStorageSettings.ApplicationSettings.Contains("userData.settings.schedule.groupfac"))
                facid = IsolatedStorageSettings.ApplicationSettings["userData.settings.schedule.groupfac"] as String;

            if (IsolatedStorageSettings.ApplicationSettings.Contains("userData.settings.schedule.groupform"))
                formid = IsolatedStorageSettings.ApplicationSettings["userData.settings.schedule.groupform"] as String;

            if (IsolatedStorageSettings.ApplicationSettings.Contains("userData.settings.schedule.hideempty"))
                HideEmptySubjectsCheckBox.IsChecked = Convert.ToBoolean(IsolatedStorageSettings.ApplicationSettings["userData.settings.schedule.hideempty"] as String);

                //MessageBox.Show(IsolatedStorageSettings.ApplicationSettings["userData.settings.schedule.group"] as String);
                //GroupLongListSelector.ItemsSource.Add("test");
                //GroupTextBox.Text = Convert.ToString(IsolatedStorageSettings.ApplicationSettings["userData.settings.schedule.group"] as String);
            
            /*
			if (IsolatedStorageSettings.ApplicationSettings.Contains("userData.settings.schedule.year"))
				YearListPicker.SelectedIndex = Convert.ToInt32(IsolatedStorageSettings.ApplicationSettings["userData.settings.schedule.year"] as String);
			if (IsolatedStorageSettings.ApplicationSettings.Contains("userData.settings.schedule.faculty"))
				FacultiesListPicker.SelectedIndex = Convert.ToInt32(IsolatedStorageSettings.ApplicationSettings["userData.settings.schedule.faculty"] as String);
			if (IsolatedStorageSettings.ApplicationSettings.Contains("userData.settings.schedule.type"))
				TypeListPicker.SelectedIndex = Convert.ToInt32(IsolatedStorageSettings.ApplicationSettings["userData.settings.schedule.type"] as String);
			if (IsolatedStorageSettings.ApplicationSettings.Contains("userData.settings.schedule.branch"))
				BranchListPicker.SelectedIndex = Convert.ToInt32(IsolatedStorageSettings.ApplicationSettings["userData.settings.schedule.branch"] as String);
            */

            if (IsolatedStorageSettings.ApplicationSettings.Contains("userData.settings.backgroundagent.enabled"))
				UseBackgroundCheckBox.IsChecked = Convert.ToBoolean(IsolatedStorageSettings.ApplicationSettings["userData.settings.backgroundagent.enabled"] as String);

            if (IsolatedStorageSettings.ApplicationSettings.Contains("userData.settings.app.useoffline"))
                UseOfflineCheckBox.IsChecked = Convert.ToBoolean(IsolatedStorageSettings.ApplicationSettings["userData.settings.app.useoffline"] as String);


            // НОООООООО!!!
            /*
            StudyCheckBox.IsEnabled = false;
            CoursesCheckBox.IsEnabled = false;
            LiveCheckBox.IsEnabled = false;
             */

			isLoadCompleated = true;
		}

		void SaveSettings()
		{
			IsolatedStorageSettings settings = IsolatedStorageSettings.ApplicationSettings;

            /*
			if (settings.Contains("userData.settings.news.study"))
				settings.Remove("userData.settings.news.study");
			settings.Add("userData.settings.news.study", Convert.ToString(StudyCheckBox.IsChecked));

			if (settings.Contains("userData.settings.news.courses"))
				settings.Remove("userData.settings.news.courses");
			settings.Add("userData.settings.news.courses", Convert.ToString(CoursesCheckBox.IsChecked));

			if (settings.Contains("userData.settings.news.live"))
				settings.Remove("userData.settings.news.live");
			settings.Add("userData.settings.news.live", Convert.ToString(LiveCheckBox.IsChecked));
             */

            if (settings.Contains("userData.settings.schedule.hideempty"))
                settings.Remove("userData.settings.schedule.hideempty");
            settings.Add("userData.settings.schedule.hideempty", Convert.ToString(HideEmptySubjectsCheckBox.IsChecked));

            if (settings.Contains("userData.settings.schedule.group"))
                settings.Remove("userData.settings.schedule.group");
            settings.Add("userData.settings.schedule.group", Convert.ToString(GroupListPicker.SelectedItem as String));

            //MessageBox.Show(facid + "\n" + formid + "\n" + group);

            if (settings.Contains("userData.settings.schedule.groupfac"))
                settings.Remove("userData.settings.schedule.groupfac");
            settings.Add("userData.settings.schedule.groupfac", facid);

            if (settings.Contains("userData.settings.schedule.groupform"))
                settings.Remove("userData.settings.schedule.groupform");
            settings.Add("userData.settings.schedule.groupform", formid);

            /*
			if (settings.Contains("userData.settings.schedule.year"))
				settings.Remove("userData.settings.schedule.year");
			settings.Add("userData.settings.schedule.year", Convert.ToString((YearListPicker.SelectedIndex)));

			if (settings.Contains("userData.settings.schedule.faculty"))
				settings.Remove("userData.settings.schedule.faculty");
			settings.Add("userData.settings.schedule.faculty", Convert.ToString(FacultiesListPicker.SelectedIndex));

			if (settings.Contains("userData.settings.schedule.type"))
				settings.Remove("userData.settings.schedule.type");
			settings.Add("userData.settings.schedule.type", Convert.ToString(TypeListPicker.SelectedIndex));

			if (settings.Contains("userData.settings.schedule.branch"))
				settings.Remove("userData.settings.schedule.branch");
			settings.Add("userData.settings.schedule.branch", Convert.ToString(BranchListPicker.SelectedIndex));
            */

			if (settings.Contains("userData.settings.backgroundagent.enabled"))
				settings.Remove("userData.settings.backgroundagent.enabled");
			settings.Add("userData.settings.backgroundagent.enabled", Convert.ToString(UseBackgroundCheckBox.IsChecked));

            if (settings.Contains("userData.settings.app.useoffline"))
                settings.Remove("userData.settings.app.useoffline");
            settings.Add("userData.settings.app.useoffline", Convert.ToString(UseOfflineCheckBox.IsChecked));

			if (settings.Contains("userData.temp.doNewsUpdate"))
				settings.Remove("userData.temp.doNewsUpdate");
			settings.Add("userData.temp.doNewsUpdate", "1");

			settings.Save();
		}

        void ResetSettings()
        {
            IsolatedStorageSettings settings = IsolatedStorageSettings.ApplicationSettings;

            /*
            if (settings.Contains("userData.settings.news.study"))
                settings.Remove("userData.settings.news.study");
            settings.Add("userData.settings.news.study", Convert.ToString(true));

            if (settings.Contains("userData.settings.news.courses"))
                settings.Remove("userData.settings.news.courses");
            settings.Add("userData.settings.news.courses", Convert.ToString(true));

            if (settings.Contains("userData.settings.news.live"))
                settings.Remove("userData.settings.news.live");
            settings.Add("userData.settings.news.live", Convert.ToString(true));
            */

            if (settings.Contains("userData.temp.doNewsUpdate"))
                settings.Remove("userData.temp.doNewsUpdate");
            settings.Add("userData.temp.doNewsUpdate", "1");

            if (settings.Contains("userData.settings.schedule.hideempty"))
                settings.Remove("userData.settings.schedule.hideempty");
            settings.Add("userData.settings.schedule.hideempty", Convert.ToString(true));

            if (settings.Contains("userData.settings.schedule.group"))
                settings.Remove("userData.settings.schedule.group");
            settings.Add("userData.settings.schedule.group", "НЕТ");

            if (settings.Contains("userData.settings.schedule.groupfac"))
                settings.Remove("userData.settings.schedule.groupfac");
            settings.Add("userData.settings.schedule.groupfac", "НЕТ");

            if (settings.Contains("userData.settings.schedule.groupform"))
                settings.Remove("userData.settings.schedule.groupform");
            settings.Add("userData.settings.schedule.groupform", "НЕТ");

            if (settings.Contains("userData.settings.backgroundagent.enabled"))
                settings.Remove("userData.settings.backgroundagent.enabled");
            settings.Add("userData.settings.backgroundagent.enabled", Convert.ToString(true));

            if (settings.Contains("userData.settings.app.useoffline"))
                settings.Remove("userData.settings.app.useoffline");
            settings.Add("userData.settings.app.useoffline", Convert.ToString(true));

            settings.Save();
        }

		private void SaveButton_Click(object sender, EventArgs e)
		{
            if (wasSettingsChanged)
            {
                SaveSettings();
            }
			NavigationService.GoBack();
		}

		private void ResetButton_Click(object sender, EventArgs e)
		{
            MessageBoxResult result = MessageBox.Show("Сбросить все настройки приложения?", "НАСТРОЙКИ", MessageBoxButton.OKCancel);

            if (result == MessageBoxResult.OK)
            {
                ResetSettings();
                LoadSettings();
                GetGroupsList();
                wasSettingsChanged = false;
            }
		}

		private void CheckBoxSettingsChanged(object sender, RoutedEventArgs e)
		{
			if (isLoadCompleated)
				wasSettingsChanged = true;
		}

		private void SettingsChanged(object sender, SelectionChangedEventArgs e)
		{
            if (isLoadCompleated && isListsLoaded && isGroupsLoaded)
            {
                wasSettingsChanged = true;
                //MessageBox.Show("" + ((RadListPicker)sender).SelectedIndex); 
            }
				
		}

        private void GroupTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            wasSettingsChanged = true;
        }

        private void GroupListPicker_Loaded(object sender, RoutedEventArgs e)
        {
            isListsLoaded = true;
        }

        private void FacListPicker_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            SettingsChanged(sender, e);
            if (isLoadCompleated && isListsLoaded && isGroupsLoaded)
            {
                int id = FacListPicker.SelectedIndex;

                facid = facsids[id][1];

                if (facid == "НЕТ")
                {
                    formid = "НЕТ";
                    FormListPicker.IsEnabled = false;
                    group = "НЕТ";
                    GroupListPicker.IsEnabled = false;
                }
                else
                {
                    FormListPicker.IsEnabled = true;
                }
                UpdateGroupSelector(data, facid, formid);
            }
        }

        private void FormListPicker_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            SettingsChanged(sender, e);
            if (isLoadCompleated && isListsLoaded && isGroupsLoaded)
            {
                int id = FormListPicker.SelectedIndex;

                formid = formsids[id][1];
                if (formid == "НЕТ")
                {
                    group = "НЕТ";
                    GroupListPicker.IsEnabled = false;
                }
                else
                {
                    GroupListPicker.IsEnabled = true;
                }
                UpdateGroupSelector(data, facid, formid);
            }
        }

        private void GroupListPicker_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            SettingsChanged(sender, e);
            if (isLoadCompleated && isListsLoaded && isGroupsLoaded)
            {
                group = GroupListPicker.SelectedItem as String;
            }
        }

        private async void DeleteButon_Click(object sender, RoutedEventArgs e)
        {
            await Helpers.RemoveAllFiles();
            Helpers.ResetTile();
            ResetSettings();
            LoadSettings();
            MessageBox.Show("Приложение было восстановлено.", "НАСТРОЙКИ", MessageBoxButton.OK);
        }
    }
}