﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Resources;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("MIREA Pulse")]
[assembly: AssemblyDescription("Пульс МИРЭА - приложение для студентов")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("KHB-Soft")]
[assembly: AssemblyProduct("MIREA_Pulse")]
[assembly: AssemblyCopyright("Copyright © KHB-Soft 2014")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("73ac90a4-cfa3-497f-839c-5b8ff7085291")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Revision and Build Numbers 
// by using the '*' as shown below:
[assembly: AssemblyVersion("1.5.2.0")]
[assembly: AssemblyFileVersion("1.5.2.0")]
[assembly: NeutralResourcesLanguageAttribute("en-US")]
