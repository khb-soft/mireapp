﻿using Microsoft.Phone.Controls;
using Microsoft.Phone.Scheduler;
using Microsoft.Phone.Shell;
using Microsoft.Phone.Tasks;
using Newtonsoft.Json;
using Resources;
using Resources.Resources;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.IO.IsolatedStorage;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Navigation;
using System.Xml.Linq;


namespace MIREA_Pulse
{
	public partial class MainPage : PhoneApplicationPage
	{
        //Downloads
		WebClient NewsDownloader = new WebClient();
		WebClient ScheduleDownloader = new WebClient();

		List<News> NewsList = new List<News>();

        //Settings
		string group = "";
		bool useBackgroundAgent = true;
        bool useOffline = true;

        //System
        ProgressIndicator prog;
        int progTasks = 0;

		// Constructor
        public MainPage()
        {
            InitializeComponent();
            LoadAppSettings();
            RegisterAgent();

            NewsDownloader.DownloadStringCompleted += new DownloadStringCompletedEventHandler(NewsDownloadCompleated);
            ScheduleDownloader.DownloadStringCompleted += new DownloadStringCompletedEventHandler(ScheduleDownloadCompleated);
            UpdateTitleStart.Completed += UpdateTitleStart_Completed;

            InitTasksSystem();
            ShowRateUsMessage();

            if (progTasks == 0)
            {
                try
                {
                    getNews();
                    getSchedule();
                    UpdateAppTitle();
                }
                catch
                { }
            }
            LoadingAnimation.Begin();
            
        }

        // Overrides
		protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
		{

            base.OnNavigatedTo(e);

			IsolatedStorageSettings settings = IsolatedStorageSettings.ApplicationSettings;

			if (settings.Contains("userData.temp.doNewsUpdate"))
			{
				settings.Remove("userData.temp.doNewsUpdate");
				settings.Save();

				LoadAppSettings();
				RegisterAgent();

                if (progTasks == 0)
                {
                    try
                    {
                        getNews(true);
                        getSchedule(false, true);
                        UpdateAppTitle();
                    }
                    catch
                    { }
                }
			}
		}

        // Functions
		void LoadAppSettings()
		{
			IsolatedStorageSettings settings = IsolatedStorageSettings.ApplicationSettings;

            /*
			if (!settings.Contains("userData.settings.news.study"))
				settings.Add("userData.settings.news.study", Convert.ToString(true));

			if (!settings.Contains("userData.settings.news.courses"))
				settings.Add("userData.settings.news.courses", Convert.ToString(true));

			if (!settings.Contains("userData.settings.news.live"))
				settings.Add("userData.settings.news.live", Convert.ToString(true));
             */

            if (!settings.Contains("userData.settings.schedule.hideempty"))
                settings.Add("userData.settings.schedule.hideempty", Convert.ToString(true));

            if (!settings.Contains("userData.settings.schedule.group"))
                settings.Add("userData.settings.schedule.group", "НЕТ");

            if (!settings.Contains("userData.settings.schedule.groupfac"))
                settings.Add("userData.settings.schedule.groupfac", "НЕТ");

            if (!settings.Contains("userData.settings.schedule.groupform"))
                settings.Add("userData.settings.schedule.groupform", "НЕТ");

			if (!settings.Contains("userData.settings.backgroundagent.enabled"))
				settings.Add("userData.settings.backgroundagent.enabled", Convert.ToString(true));

            if (!settings.Contains("userData.settings.app.useoffline"))
                settings.Add("userData.settings.app.useoffline", Convert.ToString(true));

            if (!settings.Contains("userData.settings.app.launchedTimes"))
                settings.Add("userData.settings.app.launchedTimes", Convert.ToString(1));

			settings.Save();

            if (IsolatedStorageSettings.ApplicationSettings.Contains("userData.settings.schedule.group"))
				group = Convert.ToString(IsolatedStorageSettings.ApplicationSettings["userData.settings.schedule.group"] as String);
			
            if (IsolatedStorageSettings.ApplicationSettings.Contains("userData.settings.backgroundagent.enabled"))
				useBackgroundAgent = Convert.ToBoolean(IsolatedStorageSettings.ApplicationSettings["userData.settings.backgroundagent.enabled"] as String);

            if (IsolatedStorageSettings.ApplicationSettings.Contains("userData.settings.app.useoffline"))
                useOffline = Convert.ToBoolean(IsolatedStorageSettings.ApplicationSettings["userData.settings.app.useoffline"] as String);
		}

		private void RegisterAgent()
		{
			string taskName = "PeriodicAgent";
			try
			{

				if (ScheduledActionService.Find(taskName) != null)
				{
					ScheduledActionService.Remove(taskName);
				}

				if (useBackgroundAgent)
				{
					PeriodicTask periodicTask = new PeriodicTask(taskName);
					periodicTask.Description = "Фоновое обновление новостей";
                    ScheduledActionService.Add(periodicTask);
				}

			}
			catch (InvalidOperationException exception)
			{
				MessageBox.Show(exception.Message);
			}
			catch (SchedulerServiceException schedulerException)
			{
				MessageBox.Show(schedulerException.Message);
			}
		}

		private void getTestNews()
		{
            NewsFile data = new NewsFile(new List<News>(), "0");

            data.content.Add(new News(0, DateTime.Now, 1, "Тестовый заголовок, растянутый на много строк.", "Текст", ""));
            data.content.Add(new News(0, DateTime.Now, 1, "Тестовый заголовок, растянутый на очень много строк.", "Текст", ""));
            NewsLongListSelector.ItemsSource = data.content;
		}

        void getTestSchedule()
        {
            ScheduleFile data = new ScheduleFile("ok", new List<Day>());
            data.days.Add(new Day("РАСПИСАНИЕ ЗАНЯТИЙ:", new List<Subject> {
                        new Subject("1", "9:00—10:35", "", "Первая пара", "Перерыв с 10:35 до 10:45"),
                        new Subject("2", "10:45—12:20", "", "Вторая пара", "Перерыв с 12:20 до 12:50"),
                        new Subject("3", "12:50—14:25", "", "Третяя пара", "Перерыв с 14:25 до 14:35"),
                        new Subject("4", "14:35—16:10", "", "Четвертая пара", "Перерыв с 16:10 до 16:20"),
                        new Subject("5", "16:20—17:55", "", "Пятая пара", "Перерыв с 17:55 до 18:00"),
                        //new ScheduleSubject(6, "18:00—19:35", "", "Шестая пара", ""),
                        new Subject("0", "", "", " ", "Вы можете выбрать расписание для Вашей группы в настройках приложения. А еще Вы можете создавать напоминания в календаре, указав нужную пару.")
                    }));
            ScheduleLongListSelector.ItemsSource = data.days;
        }

		private void getNews(bool offline = false)
		{
            StartNewsLoadingAnimation();
            if (Helpers.CheckNetworkConnection())
            {
                if (!offline)
                {
                    if (Helpers.CheckFile(AppResources.OfflineNewsDBFile))
                    {
                        RefreshNews(Helpers.LoadFile(AppResources.OfflineNewsDBFile));
                    }

                    Uri uri = new Uri(AppResources.NewsUrl + "&date=" + Helpers.getDate() + "&deviceid=" + Helpers.getDeviceId(), UriKind.Absolute);
                    startTask();
                    NewsDownloader.DownloadStringAsync(uri);
                }
                else
                {
                    ClearNewsList();
                    RefreshNews(Helpers.LoadFile(AppResources.OfflineNewsDBFile));
                }
            }
            else
            {
                if (!useOffline)
                {
                    ClearNewsList();
                    UpdateNewsError("Ошибка при загрузке новостей. Проверьте интернет-соединение.", "Информация о неделе недоступна");
                    StopNewsLoadingAnimation();
                }
                else
                {
                    if (Helpers.CheckFile(AppResources.OfflineNewsDBFile))
                    {
                        ClearNewsList();
                        RefreshNews(Helpers.LoadFile(AppResources.OfflineNewsDBFile));
                    }
                    else
                    {
                        ClearNewsList();
                        UpdateNewsError("Ошибка при загрузке новостей. Проверьте интернет-соединение.", "Информация о неделе недоступна");
                        StopNewsLoadingAnimation();
                    }
                }
            }
		}

		private void NewsDownloadCompleated(object sender, System.Net.DownloadStringCompletedEventArgs e)
		{
            endTask();
			try
			{
				if (e.Result == null || e.Error != null)
				{
					ClearNewsList();
                    UpdateNewsError("Ошибка при загрузке новостей. Проблемы на сервере.", "Информация о неделе недоступна");
                    StopNewsLoadingAnimation();
				}
				else
				{
					ClearNewsList();
					RefreshNews(e.Result);
				}
			}
			catch
			{
				ClearNewsList();
                UpdateNewsError("Ошибка при загрузке новостей. Проверьте интернет-соединение.", "Информация о неделе недоступна");
                StopNewsLoadingAnimation();
			}
        }

        void UpdateNewsError(string ErrorText, string ScheduleNumber)
        {
            NewsErrorTextBlock.Text = ErrorText;
            ScheduleNumberTextBlock.Text = ScheduleNumber;
        }

        void ClearNewsList()
        {
            ObservableCollection<News> itemsSource = new ObservableCollection<News>();
            itemsSource.Clear();
            NewsLongListSelector.ItemsSource = itemsSource;

            NewsList.Clear();
        }

        void sortNews(List<News> source, List<News> list)
        {
            bool study = true;
            bool courses = true;
            bool live = true;

            /*
            if (IsolatedStorageSettings.ApplicationSettings.Contains("userData.settings.news.study"))
                study = Convert.ToBoolean(IsolatedStorageSettings.ApplicationSettings["userData.settings.news.study"] as String);
            if (IsolatedStorageSettings.ApplicationSettings.Contains("userData.settings.news.courses"))
                courses = Convert.ToBoolean(IsolatedStorageSettings.ApplicationSettings["userData.settings.news.courses"] as String);
            if (IsolatedStorageSettings.ApplicationSettings.Contains("userData.settings.news.live"))
                live = Convert.ToBoolean(IsolatedStorageSettings.ApplicationSettings["userData.settings.news.live"] as String);
            */
            foreach (News news in source)
            {
                bool addToFeed = true;

                /*
                //Filters
                if (news.cat == 1 && !study)
                    addToFeed = false;
                if (news.cat == 2 && !courses)
                    addToFeed = false;
                if (news.cat == 3 && !live)
                    addToFeed = false;
                */
                if (addToFeed)
                    list.Add(news);
            }
        }

		private void RefreshNews(string jsonResult)
		{
            if (String.IsNullOrEmpty(jsonResult))
            {
                UpdateNewsError("Ошибка при чтении базы новостей. Проверьте интернет-соединение.", "Информация о неделе недоступна");
                StopNewsLoadingAnimation();
                Helpers.ResetTile();
                return;
            }
            else if (useOffline && Helpers.CheckNetworkConnection())
            {
                Task.Run(async () => {await Helpers.WriteTextFile(AppResources.OfflineNewsDBFile, jsonResult);});
            }

			NewsFile data = JsonConvert.DeserializeObject<NewsFile>(jsonResult);
            UpdateNewsError("", data.weeknumbertext);

            sortNews(data.content, NewsList);
			NewsLongListSelector.ItemsSource = NewsList;

            if (NewsList.Count > 0)
                Helpers.UpdateTile(NewsList[0]);
            else
            {
                NewsErrorTextBlock.Text = "Новости отсутствуют.";
                Helpers.ResetTile();
            }

            StopNewsLoadingAnimation();
		}

        private void getSchedule(bool forcedOffline = false, bool forcedRefresh = false)
		{
            StartScheduleLoadingAnimation();
            if (group == "НЕТ")
            {
                ClearScheduleList();
                UpdateScheduleError("");
                getTestSchedule();
                StopScheduleLoadingAnimation();
                return;
            }

            if (Helpers.CheckNetworkConnection())
            {
                if (!forcedOffline)
                {
                    if (Helpers.CheckFile(AppResources.OfflineScheduleDBFile + group) && !forcedRefresh)
                    {
                        RefreshSchedule(Helpers.LoadFile(AppResources.OfflineScheduleDBFile + group));
                    }
                    bool HideEmptySubjects = true;
                    Uri uri = new Uri(AppResources.SheduleGroupUrl + "?group=" + group + "&hideBlanc" + "&date=" + Helpers.getDate() + "&deviceid=" + Helpers.getDeviceId(), UriKind.Absolute);
                    if (IsolatedStorageSettings.ApplicationSettings.Contains("userData.settings.schedule.hideempty"))
                        HideEmptySubjects = Convert.ToBoolean(IsolatedStorageSettings.ApplicationSettings["userData.settings.schedule.hideempty"] as String);

                    if (!HideEmptySubjects)
                        uri = new Uri(AppResources.SheduleGroupUrl + "?group=" + group + "&date=" + Helpers.getDate() + "&deviceid=" + Helpers.getDeviceId(), UriKind.Absolute);
                    startTask();
                    ScheduleDownloader.DownloadStringAsync(uri);
                }
                else
                {
                    if (!forcedRefresh)
                        RefreshSchedule(Helpers.LoadFile(AppResources.OfflineScheduleDBFile+group));
                }
            }
            else
            {
                if (!useOffline)
                {
                    ClearScheduleList();
                    UpdateScheduleError("Ошибка при загрузке расписания. Проверьте интернет-соединение.");
                    StopScheduleLoadingAnimation();
                }
                else
                {
                    // Load offline DB
                    if (Helpers.CheckFile(AppResources.OfflineScheduleDBFile + group))
                    {
                        RefreshSchedule(Helpers.LoadFile(AppResources.OfflineScheduleDBFile+group));
                    }
                    else
                    {
                        ClearScheduleList();
                        UpdateScheduleError("Ошибка при загрузке расписания. Проверьте интернет-соединение.");
                        StopScheduleLoadingAnimation();
                    }
                }
            }
		}

		private void ScheduleDownloadCompleated(object sender, System.Net.DownloadStringCompletedEventArgs e)
		{
            endTask();
			try
			{
				if (e.Result == null || e.Error != null)
				{
					ClearScheduleList();
                    UpdateScheduleError("Ошибка при загрузке расписания. Проблемы на сервере. Описание:\n" + e.Error.Message);
                    StopScheduleLoadingAnimation();
				}
				else
				{
					ClearScheduleList();
					RefreshSchedule(e.Result);
				}
			}
            catch (Exception exc)
			{
				ClearScheduleList();
                UpdateScheduleError("Ошибка при загрузке расписания. Проверьте интернет-соединение. Описание:\n" + exc.Message);
                StopScheduleLoadingAnimation();
			}
		}

        void UpdateScheduleError(string ErrorText, string buttonText = "")
        {
            ScheduleErrorTextBlock.Text = ErrorText;
            if (buttonText == "")
                ScheduleHyperlinkButton.IsHitTestVisible = false;
            else
                ScheduleHyperlinkButton.IsHitTestVisible = true;
            ScheduleHyperlinkButton.Content = buttonText;
        }

        void ClearScheduleList()
        {
            ObservableCollection<News> itemsSource = new ObservableCollection<News>();
            itemsSource.Clear();
            ScheduleLongListSelector.ItemsSource = itemsSource;
        }

		private void RefreshSchedule(string jsonResult)
		{
            
            if (String.IsNullOrEmpty(jsonResult))
            {
                UpdateScheduleError("Ошибка при загрузке расписания. Проверьте интернет-соединение.");
                StopScheduleLoadingAnimation();
                return;
            }
            else if (useOffline && Helpers.CheckNetworkConnection())
            {
                Task.Run(async () => { await Helpers.WriteTextFile(AppResources.OfflineScheduleDBFile+group, jsonResult); });
            }

			ScheduleFile data = JsonConvert.DeserializeObject<ScheduleFile>(jsonResult);
            if (data.result == "ok")
            {
                data = Helpers.RefactorSchedule(data);
                ScheduleLongListSelector.ItemsSource = data.days;
            }
            else if (data.result == "empty")
                UpdateScheduleError("Расписание для группы " + group + " ещё не создано. Вы можете ", "помочь нам в этом");
            else if (data.result == "error")
                UpdateScheduleError("Произошла ошибка при запросе.");
            
            StopScheduleLoadingAnimation();
        }

        void startTask()
        {
            progTasks++;
            if (progTasks > 0)
                prog.IsVisible = true;
            else
                prog.IsVisible = false;
        }

        void endTask()
        {
            progTasks--;
            if (progTasks > 0)
                prog.IsVisible = true;
            else
                prog.IsVisible = false;
        }

        void InitTasksSystem()
        {
            SystemTray.SetIsVisible(this, true);
            prog = new ProgressIndicator();
            prog.IsIndeterminate = true;
            prog.Text = "Обновление...";
            SystemTray.SetProgressIndicator(this, prog);
            progTasks = 0;
        }

        void ClearTasks()
        {
            progTasks = 0;
            prog.IsVisible = false;
        }

        void StartNewsLoadingAnimation()
        {
            StartNewsLoading.Begin();
        }

        void StopNewsLoadingAnimation()
        {
            StopNewsLoading.Begin();
        }

        void StartScheduleLoadingAnimation()
        {
            StartScheduleLoading.Begin();
        }

        void StopScheduleLoadingAnimation()
        {
            StopScheduleLoading.Begin();
        }

        bool isInternet = false;
        void UpdateAppTitle()
        {
            if (Helpers.CheckNetworkConnection())
            {
                isInternet = true;
            }
            else
            {
                isInternet = false;
            }

            UpdateTitleStart.Begin();
        }

        private void UpdateTitleStart_Completed(object sender, EventArgs e)
        {
            if (isInternet)
            {
                AppTitleTextBlock.Text = "ПУЛЬС МИРЭА";
            }
            else
            {
                AppTitleTextBlock.Text = "ПУЛЬС МИРЭА [offline]";
            }

            UpdateTitleEnd.Begin();
        }

        void ShowRateUsMessage()
        {
            int launched = 0;
            if (IsolatedStorageSettings.ApplicationSettings.Contains("userData.settings.app.launchedTimes"))
            {
                launched = Convert.ToInt32(IsolatedStorageSettings.ApplicationSettings["userData.settings.app.launchedTimes"] as String);
                launched++;
                IsolatedStorageSettings.ApplicationSettings.Remove("userData.settings.app.launchedTimes");
                IsolatedStorageSettings.ApplicationSettings.Add("userData.settings.app.launchedTimes", Convert.ToString(launched));
                IsolatedStorageSettings.ApplicationSettings.Save();

                if (launched == 1)
                {
                    MessageBoxResult returnvalue = MessageBox.Show("Это приложение использует интернет-соединение для передачи данных. Рекомендуем использовать Wi-Fi, чтобы снизить затраты на передачу данных.", "Внимание!", MessageBoxButton.OK);
                }

                if (launched == 5)
                {
                    MessageBoxResult returnvalue = MessageBox.Show("Спасибо за то, что скачали это приложение! Если оно вам понравилась, пожалуйста, оцените его и оставьте отзыв.", "Нам важно Ваше мнение!", MessageBoxButton.OKCancel);
                    if (returnvalue == MessageBoxResult.OK)
                    {
                        var marketplaceReviewTask = new MarketplaceReviewTask();
                        marketplaceReviewTask.Show();
                    }
                }
            }
        }

        //Events
		private void NewsLongListSelector_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			if (NewsLongListSelector.SelectedItem == null)
				return;

			News selectedNews = ((ListView)sender).SelectedItem as News;
			//MessageBox.Show(selectedNews.title);
			NavigationService.Navigate(new Uri("/DetailsPage.xaml?selectedItem=" + selectedNews.newsid, UriKind.Relative));

			NewsLongListSelector.SelectedItem = null;
		}

		private void settings_Click(object sender, EventArgs e)
		{
			NavigationService.Navigate(new Uri("/SettingsPage.xaml", UriKind.Relative));
		}

		private void about_Click(object sender, EventArgs e)
		{
			//checkBrowserVisibility();
            MessageBox.Show("Официальное приложение для студентов и аспирантов МГТУ МИРЭА.\n\nРазработчик: KHB-Soft\nПрограммист: Oxy949\nВерсия: " + Helpers.getApplicationVersion() + "\nРазработано совместно с iStudio\nДля связи: oxy949@gmail.com", "ПУЛЬС МИРЭА", MessageBoxButton.OK);
		}

		private void refresh_Click(object sender, EventArgs e)
		{
            if (progTasks == 0)
            {
                try
                {
                    getNews();
                    getSchedule(false, true);
                    UpdateAppTitle();
                }
                catch
                { }
            }
		}

        private void Subject_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (((ListView)sender).SelectedItem == null)
				return;

            Subject selected = ((ListView)sender).SelectedItem as Subject;
            //MessageBox.Show(selected.day);
            int index = selected.Time.LastIndexOf("—");
            string start = "0";
            string end = "0";
            if (index > 0)
            {
                start = selected.Time.Substring(0, index);
                //MessageBox.Show(start);
                end = selected.Time.Substring(index + 1);
                //MessageBox.Show(end);
            }

            DateTime today = DateTime.Today;
            DayOfWeek dow = DayOfWeek.Monday;
            switch (selected.day.ToUpper())
            {
                case ("ПОНЕДЕЛЬНИК"):
                    dow = DayOfWeek.Monday;
                    break;
                case ("ВТОРНИК"):
                    dow = DayOfWeek.Tuesday;
                    break;
                case ("СРЕДА"):
                    dow = DayOfWeek.Wednesday;
                    break;
                case ("ЧЕТВЕРГ"):
                    dow = DayOfWeek.Thursday;
                    break;
                case ("ПЯТНИЦА"):
                    dow = DayOfWeek.Friday;
                    break;
                case ("СУББОТА"):
                    dow = DayOfWeek.Sunday;
                    break;
                case ("ВОСКРЕСЕНЬЕ"):
                    dow = DayOfWeek.Sunday;
                    break;
                default:
                    dow = today.AddDays(1).DayOfWeek;
                    break;
            }
            int daysUntilDow = ((int)dow - (int)today.DayOfWeek + 7) % 7;
            DateTime nextDow = today.AddDays(daysUntilDow);

            try
            {
                SaveAppointmentTask saveAppointmentTask = new SaveAppointmentTask();
                saveAppointmentTask.StartTime = DateTime.Parse(start).AddDays(daysUntilDow);
                saveAppointmentTask.EndTime = DateTime.Parse(end).AddDays(daysUntilDow);
                saveAppointmentTask.Subject = "" + selected.Name;
                saveAppointmentTask.Location = "МГТУ МИРЭА, " + selected.ClassNum;
                saveAppointmentTask.Details = "Ведет: " + selected.Professor;
                saveAppointmentTask.IsAllDayEvent = false;
                saveAppointmentTask.Reminder = Microsoft.Phone.Tasks.Reminder.FifteenMinutes;
                saveAppointmentTask.Show();
            }
            catch
            { }
            ((ListView)sender).SelectedItem = null;
        }

        private void ScheduleEditorApplicationBarMenuItem_Click(object sender, EventArgs e)
        {
            Uri uri = new Uri("http://app.mirea.ru/schedule/editor/index.php");
            WebBrowserTask webBrowserTask = new WebBrowserTask();
            webBrowserTask.Uri = uri;
            webBrowserTask.Show();
        }

        private void ScheduleHyperlinkButton_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            Uri uri = new Uri("http://app.mirea.ru/help/");
            WebBrowserTask webBrowserTask = new WebBrowserTask();
            webBrowserTask.Uri = uri;
            webBrowserTask.Show();
        }

        private void LongListSelector_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

            if (((ListView)sender).SelectedItem == null)
                return;

            Subject selected = ((ListView)sender).SelectedItem as Subject;
            //MessageBox.Show(selected.day);
            int index = selected.Time.LastIndexOf("—");
            string start = "0";
            string end = "0";
            if (index > 0)
            {
                start = selected.Time.Substring(0, index);
                //MessageBox.Show(start);
                end = selected.Time.Substring(index + 1);
                //MessageBox.Show(end);
            }

            DateTime today = DateTime.Today;
            DayOfWeek dow = DayOfWeek.Monday;
            switch (selected.day.ToUpper())
            {
                case ("ПОНЕДЕЛЬНИК"):
                    dow = DayOfWeek.Monday;
                    break;
                case ("ВТОРНИК"):
                    dow = DayOfWeek.Tuesday;
                    break;
                case ("СРЕДА"):
                    dow = DayOfWeek.Wednesday;
                    break;
                case ("ЧЕТВЕРГ"):
                    dow = DayOfWeek.Thursday;
                    break;
                case ("ПЯТНИЦА"):
                    dow = DayOfWeek.Friday;
                    break;
                case ("СУББОТА"):
                    dow = DayOfWeek.Sunday;
                    break;
                case ("ВОСКРЕСЕНЬЕ"):
                    dow = DayOfWeek.Sunday;
                    break;
                default:
                    dow = today.AddDays(1).DayOfWeek;
                    break;
            }
            int daysUntilDow = ((int)dow - (int)today.DayOfWeek + 7) % 7;
            DateTime nextDow = today.AddDays(daysUntilDow);

            try
            {
                SaveAppointmentTask saveAppointmentTask = new SaveAppointmentTask();
                saveAppointmentTask.StartTime = DateTime.Parse(start).AddDays(daysUntilDow);
                saveAppointmentTask.EndTime = DateTime.Parse(end).AddDays(daysUntilDow);
                saveAppointmentTask.Subject = "" + selected.Name;
                saveAppointmentTask.Location = "МГТУ МИРЭА";
                if (!String.IsNullOrWhiteSpace(selected.ClassNum))
                    saveAppointmentTask.Location = saveAppointmentTask.Location + ", " + selected.ClassNum;
                if (!String.IsNullOrWhiteSpace(selected.Professor))
                    saveAppointmentTask.Details = "" + selected.Professor;
                saveAppointmentTask.Details = saveAppointmentTask.Details + "\n\nНапоминание создано с помощью ПУЛЬС МИРЭА.";
                saveAppointmentTask.IsAllDayEvent = false;
                saveAppointmentTask.Reminder = Microsoft.Phone.Tasks.Reminder.FifteenMinutes;
                saveAppointmentTask.Show();
            }
            catch
            { }
            ((ListView)sender).SelectedItem = null;
        
        }
	}
}