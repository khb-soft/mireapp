﻿using Microsoft.Phone.Scheduler;
using Microsoft.Phone.Shell;
using Newtonsoft.Json;
using Resources;
using Resources.Resources;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO.IsolatedStorage;
using System.Linq;
using System.Net;
using System.Windows;

namespace ScheduledTaskAgent1
{
	public class ScheduledAgent : ScheduledTaskAgent
	{
		WebClient NewsDownloader = new WebClient();
		List<News> NewsList = new List<News>();

		/// <remarks>
		/// ScheduledAgent constructor, initializes the UnhandledException handler
		/// </remarks>
		static ScheduledAgent()
		{
			// Subscribe to the managed exception handler
			Deployment.Current.Dispatcher.BeginInvoke(delegate
			{
				Application.Current.UnhandledException += UnhandledException;
			});
		}

		/// Code to execute on Unhandled Exceptions
		private static void UnhandledException(object sender, ApplicationUnhandledExceptionEventArgs e)
		{
			if (Debugger.IsAttached)
			{
				// An unhandled exception has occurred; break into the debugger
				Debugger.Break();
			}
		}

		private void getNews()
		{
			NewsDownloader.DownloadStringCompleted += new DownloadStringCompletedEventHandler(NewsDownloadCompleated);
			try
			{
                Uri uri = new Uri(AppResources.NewsUrl + "&date=" + Helpers.getDate() + "&deviceid=" + Helpers.getDeviceId(), UriKind.Absolute);
				NewsDownloader.DownloadStringAsync(uri);
			}
			catch
			{ }
		}

		private void NewsDownloadCompleated(object sender, System.Net.DownloadStringCompletedEventArgs e)
		{
			try
			{
				if (e.Result == null || e.Error != null)
				{
					NotifyComplete();
				}
				else
				{
					RefreshNews(e.Result);
				}
			}
			catch
			{
				NotifyComplete();
			}
		}

		private void RefreshNews(String jsonResult)
		{

			NewsList.Clear();

			NewsFile data = JsonConvert.DeserializeObject<NewsFile>(jsonResult);
            /*
			bool study = true;
			bool courses = true;
			bool live = true;


			if (IsolatedStorageSettings.ApplicationSettings.Contains("userData.settings.news.study"))
				study = Convert.ToBoolean(IsolatedStorageSettings.ApplicationSettings["userData.settings.news.study"] as String);
			if (IsolatedStorageSettings.ApplicationSettings.Contains("userData.settings.news.courses"))
				courses = Convert.ToBoolean(IsolatedStorageSettings.ApplicationSettings["userData.settings.news.courses"] as String);
			if (IsolatedStorageSettings.ApplicationSettings.Contains("userData.settings.news.live"))
				live = Convert.ToBoolean(IsolatedStorageSettings.ApplicationSettings["userData.settings.news.live"] as String);

			if (!(!study && !courses && !live))
			{*/
				foreach (News news in data.content)
				{
					bool addToFeed = true;
                    /*
					//Filters
					if (news.cat == 1 && !study)
						addToFeed = false;
					if (news.cat == 2 && !courses)
						addToFeed = false;
					if (news.cat == 3 && !live)
						addToFeed = false;
                    */
					if (addToFeed)
						NewsList.Add(news);
				}
			/*}*/

			if (NewsList.Count > 0)
				Helpers.UpdateTile(NewsList[0]);
			else
				Helpers.ResetTile();

			NotifyComplete();
		}

		/// <summary>
		/// Agent that runs a scheduled task
		/// </summary>
		/// <param name="task">
		/// The invoked task
		/// </param>
		/// <remarks>
		/// This method is called when a periodic or resource intensive task is invoked
		/// </remarks>
		protected override void OnInvoke(ScheduledTask task)
		{
			if (task is PeriodicTask)
			{
				//ShellToast toast = new ShellToast();
				//toast.Title = "Background Agent Sample";
				//toast.Content = "OnInvoke";
				//toast.Show();
				getNews();
				//ScheduledActionService.LaunchForTest(task.Name, TimeSpan.FromSeconds(20));
			}

			// If debugging is enabled, launch the agent again in one minute.
//#if DEBUG
//  ScheduledActionService.LaunchForTest(task.Name, TimeSpan.FromSeconds(60));
//#endif

			// Call NotifyComplete to let the system know the agent is done working.
			//NotifyComplete();
		}
	}
}